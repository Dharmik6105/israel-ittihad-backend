import { INestApplication } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

export const setupSwagger = (app: INestApplication) => {
    const config = new DocumentBuilder()
        .setTitle('Israel-Ittihad Commnet')
        .setDescription('Swagger documentation for Israel-Ittihad Commnet')
        .setVersion('1.0')
        .addBearerAuth(undefined, 'JWT token')
        .build();
    const document = SwaggerModule.createDocument(app, config);
    const options = {
        swaggerOptions: {
            authAction: {
                defaultBearerAuth: {
                    name: 'JWT token',
                    schema: {
                        description: 'Default',
                        type: 'http',
                        in: 'header',
                        scheme: 'bearer',
                        bearerFormat: 'JWT',
                    },
                },
            },
        },
    };
    SwaggerModule.setup('api', app, document, options);
};
