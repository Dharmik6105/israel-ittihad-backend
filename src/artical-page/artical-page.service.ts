import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import mongoose, { Model } from 'mongoose';
import { Comments } from 'src/comments/schema/comment.schema';
import { CommentReplay } from 'src/comments/schema/commentReplay.schema';
import { CommentLike } from 'src/comments/schema/commentsLike.schema';
import { decrypt } from 'src/helpers/encrypt-decrypt';
import { paginationObject, returnMessage } from 'src/helpers/utils';
import { ICommentRep } from 'src/interfaces/commentRep.interface';
import { IComment } from 'src/interfaces/comments.interface';
import { PageSettingDocument, page_setting } from 'src/page-setting/page-setting.schema';
import { IPage } from 'src/page-setting/setting.interface';
import { Page, PageDocument } from 'src/pages/pages.schema';
import { ArticlePageUser, ArticlePageUserDocument } from 'src/user/schema/article-page-user.schema';
import { UserService } from 'src/user/user.service';

@Injectable()
export class ArticalPageService {
    constructor(
        @InjectModel(page_setting.name, 'SYSTEM_DB') private PageSettingModel: Model<PageSettingDocument>,
        @InjectModel(Page.name, 'SYSTEM_DB') private pageModel: Model<PageDocument>,
        @InjectModel(CommentLike.name, 'SYSTEM_DB') private CommentLikeModel: Model<IComment>,
        @InjectModel(Comments.name, 'SYSTEM_DB') private commentModel: Model<IComment>,
        @InjectModel(ArticlePageUser.name, 'ISRAEL_DB') private israelUserModel: Model<ArticlePageUserDocument>,
        @InjectModel(ArticlePageUser.name, 'ITTIHAD_DB') private ittihadUserModel: Model<ArticlePageUserDocument>,
        @InjectModel(CommentReplay.name, 'SYSTEM_DB') private commentReplayModel: Model<ICommentRep>,
        private readonly userService: UserService,
    ) {}

    async getArticlePage(request: any, pageId: any, searchObject: any, userId: any, site: any) {
        try {
            if (!site) return returnMessage('siteNotFound');
            if (!pageId) return returnMessage('pageNotFound');

            const verifyPage = await this.pageModel.findById(pageId).lean();

            if (!verifyPage) return returnMessage('pageNotFound');

            if (site === 'israelBackOffice') {
                if (verifyPage.israelStatus !== 'active') return returnMessage('pageNotApproved');
                if (verifyPage.isrealUrl !== request.body.url) return returnMessage('pageAndUrlMismatch');
            } else if (site === 'ittihadBackOffice') {
                if (verifyPage.ittihadStatus !== 'active') return returnMessage('pageNotApproved');
                if (verifyPage.ittihadUrl !== request.body.url) return returnMessage('pageAndUrlMismatch');
            }

            if (request.headers.authorization && request.headers.authorization.startsWith('Bearer')) {
                const token = request.headers.authorization.split(' ')[1];
                const user = await this.userService.verifyArticlePageAuthToken(token);
                if (typeof user === 'string') return user;
                request['user'] = user;
            }
            let pagePipeline = {
                _id: 1,
                top_banner_image: 1,
                logo_image: 1,
                login_image: 1,
                top_title: 1,
                sub_title: 1,
                footer_text: 1,
                terms_privacy_policy: 1,
                pipeline: 1,
                mustLogin: 1,
                google_client_id: 1,
                confirmCommentPopUpMessage: 1,
            };
            let queryObject = {};
            const pageData = await this.PageSettingModel.findOne({ site }).select(pagePipeline).lean();
            if (site === 'israelBackOffice') queryObject['israelStatus'] = 'approved';
            else if (site === 'ittihadBackOffice') queryObject['ittihadStatus'] = 'approved';

            let siteLogo = site === 'israelBackOffice' ? 'ittihadBackOffice' : 'israelBackOffice';

            const pageLogoData = await this.PageSettingModel.findOne({ site: siteLogo }).select('logo_image');
            const pageLogo = pageLogoData.logo_image;

            pageData['logo_image_2'] = pageLogo;

            let pagination: any = paginationObject(searchObject);

            if (pageId) {
                queryObject['pageId'] = new mongoose.Types.ObjectId(pageId);
            }

            const branches = [
                {
                    case: {
                        $and: [
                            { $eq: [site, 'israelBackOffice'] },
                            { $eq: ['$site', 'israel-today'] },
                            {
                                $ne: [{ $ifNull: ['$updatedCommentByIsrael', 'undefined'] }, 'undefined'],
                            },
                        ],
                    },
                    then: {
                        dd: 259,
                        comment: '$updatedCommentByIsrael',
                    },
                },
                {
                    case: {
                        $and: [
                            { $eq: [site, 'israelBackOffice'] },
                            { $eq: ['$site', 'israel-today'] },
                            {
                                $eq: [{ $ifNull: ['$updatedCommentByIsrael', 'undefined'] }, 'undefined'],
                            },
                        ],
                    },
                    then: {
                        dd: 274,
                        comment: '$originalComment',
                    },
                },
                {
                    case: {
                        $and: [
                            { $eq: [site, 'ittihadBackOffice'] },
                            { $eq: ['$site', 'ittihad-today'] },
                            {
                                $ne: [{ $ifNull: ['$updatedCommentByIttihad', 'undefined'] }, 'undefined'],
                            },
                        ],
                    },
                    then: {
                        dd: 289,
                        comment: '$updatedCommentByIttihad',
                    },
                },
                {
                    case: {
                        $and: [
                            { $eq: [site, 'ittihadBackOffice'] },
                            { $eq: ['$site', 'ittihad-today'] },
                            {
                                $eq: [{ $ifNull: ['$updatedCommentByIttihad', 'undefined'] }, 'undefined'],
                            },
                        ],
                    },
                    then: {
                        dd: 304,
                        comment: '$originalComment',
                    },
                },
                {
                    case: {
                        $and: [
                            { $eq: [site, 'israelBackOffice'] },
                            { $eq: ['$site', 'ittihad-today'] },
                            {
                                $ne: [{ $ifNull: ['$updatedCommentByIsrael', 'undefined'] }, 'undefined'],
                            },
                            {
                                $ne: [{ $ifNull: ['$updatedCommentByIttihad', 'undefined'] }, 'undefined'],
                            },
                        ],
                    },
                    then: {
                        dd: 322,
                        comment: '$updatedCommentByIsrael',
                        originalComment: '$updatedCommentByIttihad',
                    },
                },
                {
                    case: {
                        $and: [
                            { $eq: [site, 'israelBackOffice'] },
                            { $eq: ['$site', 'ittihad-today'] },
                            {
                                $ne: [{ $ifNull: ['$updatedCommentByIsrael', 'undefined'] }, 'undefined'],
                            },
                            {
                                $eq: [{ $ifNull: ['$updatedCommentByIttihad', 'undefined'] }, 'undefined'],
                            },
                        ],
                    },
                    then: {
                        dd: 341,
                        comment: '$updatedCommentByIsrael',
                        originalComment: '$originalComment',
                    },
                },
                {
                    case: {
                        $and: [
                            { $eq: [site, 'israelBackOffice'] },
                            { $eq: ['$site', 'ittihad-today'] },
                            {
                                $eq: [{ $ifNull: ['$updatedCommentByIsrael', 'undefined'] }, 'undefined'],
                            },
                            {
                                $ne: [{ $ifNull: ['$updatedCommentByIttihad', 'undefined'] }, 'undefined'],
                            },
                        ],
                    },
                    then: {
                        dd: 360,
                        comment: '$ittihadTranslation',
                        originalComment: '$updatedCommentByIttihad',
                    },
                },
                {
                    case: {
                        $and: [
                            { $eq: [site, 'israelBackOffice'] },
                            { $eq: ['$site', 'ittihad-today'] },
                            {
                                $eq: [{ $ifNull: ['$updatedCommentByIsrael', 'undefined'] }, 'undefined'],
                            },
                            {
                                $eq: [{ $ifNull: ['$updatedCommentByIttihad', 'undefined'] }, 'undefined'],
                            },
                        ],
                    },
                    then: {
                        dd: 379,
                        comment: '$ittihadTranslation',
                        originalComment: '$originalComment',
                    },
                },
                {
                    case: {
                        $and: [
                            { $eq: [site, 'ittihadBackOffice'] },
                            { $eq: ['$site', 'israel-today'] },
                            {
                                $ne: [{ $ifNull: ['$updatedCommentByIsrael', 'undefined'] }, 'undefined'],
                            },
                            {
                                $ne: [{ $ifNull: ['$updatedCommentByIttihad', 'undefined'] }, 'undefined'],
                            },
                        ],
                    },
                    then: {
                        dd: 398,
                        comment: '$updatedCommentByIttihad',
                        originalComment: '$updatedCommentByIsrael',
                    },
                },
                {
                    case: {
                        $and: [
                            { $eq: [site, 'ittihadBackOffice'] },
                            { $eq: ['$site', 'israel-today'] },
                            {
                                $ne: [{ $ifNull: ['$updatedCommentByIttihad', 'undefined'] }, 'undefined'],
                            },
                            {
                                $eq: [{ $ifNull: ['$updatedCommentByIsrael', 'undefined'] }, 'undefined'],
                            },
                        ],
                    },
                    then: {
                        dd: 417,
                        comment: '$updatedCommentByIttihad',
                        originalComment: '$originalComment',
                    },
                },
                {
                    case: {
                        $and: [
                            { $eq: [site, 'ittihadBackOffice'] },
                            { $eq: ['$site', 'israel-today'] },
                            {
                                $eq: [{ $ifNull: ['$updatedCommentByIttihad', 'undefined'] }, 'undefined'],
                            },
                            {
                                $ne: [{ $ifNull: ['$updatedCommentByIsrael', 'undefined'] }, 'undefined'],
                            },
                        ],
                    },
                    then: {
                        dd: 436,
                        comment: '$israelTranslation',
                        originalComment: '$updatedCommentByIsrael',
                    },
                },
                {
                    case: {
                        $and: [
                            { $eq: [site, 'ittihadBackOffice'] },
                            { $eq: ['$site', 'israel-today'] },
                            {
                                $eq: [{ $ifNull: ['$updatedCommentByIsrael', 'undefined'] }, 'undefined'],
                            },
                            {
                                $eq: [{ $ifNull: ['$updatedCommentByIttihad', 'undefined'] }, 'undefined'],
                            },
                        ],
                    },
                    then: {
                        dd: 455,
                        comment: '$israelTranslation',
                        originalComment: '$originalComment',
                    },
                },
            ];

            const replyBranches = [
                {
                    case: {
                        $and: [
                            { $eq: [site, 'israelBackOffice'] },
                            { $eq: ['$$reply.site', 'israel-today'] },
                            { $eq: ['$$reply.israelStatus', 'approved'] },
                            {
                                $ne: [{ $ifNull: ['$$reply.updatedCommentByIsrael', 'undefined'] }, 'undefined'],
                            },
                        ],
                    },
                    then: {
                        dd: 475,
                        comment: { comment: '$$reply.updatedCommentByIsrael' },
                        id: '$$reply._id',
                        site: '$$reply.site',
                        ip: '$$reply.ip',
                        userId: '$$reply.userId',
                        likeCount: { $size: '$$reply.like' },
                        createdAt: '$$reply.createdAt',
                        israelStatus: '$$reply.israelStatus',
                    },
                },
                {
                    case: {
                        $and: [
                            { $eq: [site, 'israelBackOffice'] },
                            { $eq: ['$$reply.site', 'israel-today'] },
                            { $eq: ['$$reply.israelStatus', 'approved'] },
                            {
                                $eq: [{ $ifNull: ['$$reply.updatedCommentByIsrael', 'undefined'] }, 'undefined'],
                            },
                        ],
                    },
                    then: {
                        dd: 497,
                        comment: { comment: '$$reply.originalComment' },
                        id: '$$reply._id',
                        site: '$$reply.site',
                        ip: '$$reply.ip',
                        userId: '$$reply.userId',
                        likeCount: { $size: '$$reply.like' },
                        createdAt: '$$reply.createdAt',
                        israelStatus: '$$reply.israelStatus',
                    },
                },
                {
                    case: {
                        $and: [
                            { $eq: [site, 'ittihadBackOffice'] },
                            { $eq: ['$$reply.site', 'ittihad-today'] },
                            { $eq: ['$$reply.ittihadStatus', 'approved'] },
                            {
                                $ne: [{ $ifNull: ['$$reply.updatedCommentByIttihad', 'undefined'] }, 'undefined'],
                            },
                        ],
                    },
                    then: {
                        dd: 519,
                        comment: { comment: '$$reply.updatedCommentByIttihad' },
                        id: '$$reply._id',
                        site: '$$reply.site',
                        ip: '$$reply.ip',
                        userId: '$$reply.userId',
                        likeCount: { $size: '$$reply.like' },
                        createdAt: '$$reply.createdAt',
                        ittihadStatus: '$$reply.ittihadStatus',
                    },
                },
                {
                    case: {
                        $and: [
                            { $eq: [site, 'ittihadBackOffice'] },
                            { $eq: ['$$reply.site', 'ittihad-today'] },
                            { $eq: ['$$reply.ittihadStatus', 'approved'] },
                            {
                                $eq: [{ $ifNull: ['$$reply.updatedCommentByIttihad', 'undefined'] }, 'undefined'],
                            },
                        ],
                    },
                    then: {
                        dd: 541,
                        comment: { comment: '$$reply.originalComment' },
                        id: '$$reply._id',
                        site: '$$reply.site',
                        ip: '$$reply.ip',
                        userId: '$$reply.userId',
                        likeCount: { $size: '$$reply.like' },
                        createdAt: '$$reply.createdAt',
                        ittihadStatus: '$$reply.ittihadStatus',
                    },
                },
                {
                    case: {
                        $and: [
                            { $eq: [site, 'israelBackOffice'] },
                            { $eq: ['$$reply.site', 'ittihad-today'] },
                            { $eq: ['$$reply.israelStatus', 'approved'] },
                            {
                                $ne: [{ $ifNull: ['$$reply.updatedCommentByIsrael', 'undefined'] }, 'undefined'],
                            },
                            {
                                $ne: [{ $ifNull: ['$$reply.updatedCommentByIttihad', 'undefined'] }, 'undefined'],
                            },
                        ],
                    },
                    then: {
                        dd: 566,
                        comment: {
                            comment: '$$reply.updatedCommentByIsrael',
                            originalComment: '$$reply.updatedCommentByIttihad',
                        },
                        id: '$$reply._id',
                        site: '$$reply.site',
                        ip: '$$reply.ip',
                        userId: '$$reply.userId',
                        likeCount: { $size: '$$reply.like' },
                        createdAt: '$$reply.createdAt',
                        israelStatus: '$$reply.israelStatus',
                    },
                },
                {
                    case: {
                        $and: [
                            { $eq: [site, 'israelBackOffice'] },
                            { $eq: ['$$reply.site', 'ittihad-today'] },
                            { $eq: ['$$reply.israelStatus', 'approved'] },
                            {
                                $ne: [{ $ifNull: ['$$reply.updatedCommentByIsrael', 'undefined'] }, 'undefined'],
                            },
                            {
                                $eq: [{ $ifNull: ['$$reply.updatedCommentByIttihad', 'undefined'] }, 'undefined'],
                            },
                        ],
                    },
                    then: {
                        dd: 594,
                        comment: {
                            comment: '$$reply.updatedCommentByIsrael',
                            originalComment: '$$reply.originalComment',
                        },
                        id: '$$reply._id',
                        site: '$$reply.site',
                        ip: '$$reply.ip',
                        userId: '$$reply.userId',
                        likeCount: { $size: '$$reply.like' },
                        createdAt: '$$reply.createdAt',
                        israelStatus: '$$reply.israelStatus',
                    },
                },
                {
                    case: {
                        $and: [
                            { $eq: [site, 'israelBackOffice'] },
                            { $eq: ['$$reply.site', 'ittihad-today'] },
                            { $eq: ['$$reply.israelStatus', 'approved'] },
                            {
                                $eq: [{ $ifNull: ['$$reply.updatedCommentByIsrael', 'undefined'] }, 'undefined'],
                            },
                            {
                                $ne: [{ $ifNull: ['$$reply.updatedCommentByIttihad', 'undefined'] }, 'undefined'],
                            },
                        ],
                    },
                    then: {
                        dd: 622,
                        comment: {
                            comment: '$$reply.ittihadTranslation',
                            originalComment: '$$reply.updatedCommentByIttihad',
                        },
                        id: '$$reply._id',
                        site: '$$reply.site',
                        ip: '$$reply.ip',
                        userId: '$$reply.userId',
                        likeCount: { $size: '$$reply.like' },
                        createdAt: '$$reply.createdAt',
                        israelStatus: '$$reply.israelStatus',
                    },
                },
                {
                    case: {
                        $and: [
                            { $eq: [site, 'israelBackOffice'] },
                            { $eq: ['$$reply.site', 'ittihad-today'] },
                            { $eq: ['$$reply.israelStatus', 'approved'] },
                            {
                                $eq: [{ $ifNull: ['$$reply.updatedCommentByIsrael', 'undefined'] }, 'undefined'],
                            },
                            {
                                $eq: [{ $ifNull: ['$$reply.updatedCommentByIttihad', 'undefined'] }, 'undefined'],
                            },
                        ],
                    },
                    then: {
                        dd: 650,
                        comment: {
                            comment: '$$reply.ittihadTranslation',
                            originalComment: '$$reply.originalComment',
                        },
                        id: '$$reply._id',
                        site: '$$reply.site',
                        ip: '$$reply.ip',
                        userId: '$$reply.userId',
                        likeCount: { $size: '$$reply.like' },
                        createdAt: '$$reply.createdAt',
                        israelStatus: '$$reply.israelStatus',
                    },
                },
                {
                    case: {
                        $and: [
                            { $eq: [site, 'ittihadBackOffice'] },
                            { $eq: ['$$reply.site', 'israel-today'] },
                            { $eq: ['$$reply.ittihadStatus', 'approved'] },
                            {
                                $ne: [{ $ifNull: ['$$reply.updatedCommentByIsrael', 'undefined'] }, 'undefined'],
                            },
                            {
                                $ne: [{ $ifNull: ['$$reply.updatedCommentByIttihad', 'undefined'] }, 'undefined'],
                            },
                        ],
                    },
                    then: {
                        dd: 675,
                        comment: {
                            comment: '$$reply.updatedCommentByIttihad',
                            originalComment: '$$reply.updatedCommentByIsrael',
                        },
                        id: '$$reply._id',
                        site: '$$reply.site',
                        ip: '$$reply.ip',
                        userId: '$$reply.userId',
                        likeCount: { $size: '$$reply.like' },
                        createdAt: '$$reply.createdAt',
                        ittihadStatus: '$$reply.ittihadStatus',
                    },
                },
                {
                    case: {
                        $and: [
                            { $eq: [site, 'ittihadBackOffice'] },
                            { $eq: ['$$reply.site', 'israel-today'] },
                            { $eq: ['$$reply.ittihadStatus', 'approved'] },
                            {
                                $ne: [{ $ifNull: ['$$reply.updatedCommentByIttihad', 'undefined'] }, 'undefined'],
                            },
                            {
                                $eq: [{ $ifNull: ['$$reply.updatedCommentByIsrael', 'undefined'] }, 'undefined'],
                            },
                        ],
                    },
                    then: {
                        dd: 703,
                        comment: {
                            comment: '$$reply.updatedCommentByIttihad',
                            originalComment: '$$reply.originalComment',
                        },
                        id: '$$reply._id',
                        site: '$$reply.site',
                        ip: '$$reply.ip',
                        userId: '$$reply.userId',
                        likeCount: { $size: '$$reply.like' },
                        createdAt: '$$reply.createdAt',
                        ittihadStatus: '$$reply.ittihadStatus',
                    },
                },
                {
                    case: {
                        $and: [
                            { $eq: [site, 'ittihadBackOffice'] },
                            { $eq: ['$$reply.site', 'israel-today'] },
                            { $eq: ['$$reply.ittihadStatus', 'approved'] },
                            {
                                $eq: [{ $ifNull: ['$$reply.updatedCommentByIttihad', 'undefined'] }, 'undefined'],
                            },
                            {
                                $ne: [{ $ifNull: ['$$reply.updatedCommentByIsrael', 'undefined'] }, 'undefined'],
                            },
                        ],
                    },
                    then: {
                        dd: 731,
                        comment: {
                            comment: '$$reply.israelTranslation',
                            originalComment: '$$reply.updatedCommentByIsrael',
                        },
                        id: '$$reply._id',
                        site: '$$reply.site',
                        ip: '$$reply.ip',
                        userId: '$$reply.userId',
                        likeCount: { $size: '$$reply.like' },
                        createdAt: '$$reply.createdAt',
                        ittihadStatus: '$$reply.ittihadStatus',
                    },
                },
                {
                    case: {
                        $and: [
                            { $eq: [site, 'ittihadBackOffice'] },
                            { $eq: ['$$reply.site', 'israel-today'] },
                            { $eq: ['$$reply.ittihadStatus', 'approved'] },
                            {
                                $eq: [{ $ifNull: ['$$reply.updatedCommentByIsrael', 'undefined'] }, 'undefined'],
                            },
                            {
                                $eq: [{ $ifNull: ['$$reply.updatedCommentByIttihad', 'undefined'] }, 'undefined'],
                            },
                        ],
                    },
                    then: {
                        dd: 759,
                        comment: {
                            comment: '$$reply.israelTranslation',
                            originalComment: '$$reply.originalComment',
                        },
                        id: '$$reply._id',
                        site: '$$reply.site',
                        ip: '$$reply.ip',
                        userId: '$$reply.userId',
                        likeCount: { $size: '$$reply.like' },
                        createdAt: '$$reply.createdAt',
                        ittihadStatus: '$$reply.ittihadStatus',
                    },
                },
            ];

            const pipeline = [
                {
                    $lookup: {
                        from: 'commentreplays',
                        localField: 'replyComments',
                        foreignField: '_id',
                        as: 'replyComments',
                    },
                },
                {
                    $lookup: {
                        from: 'pages',
                        localField: 'pageId',
                        foreignField: '_id',
                        as: 'pageData',
                        pipeline: [{ $project: { israelPage: 1, ittihadPage: 1 } }],
                    },
                },
                { $unwind: '$pageData' },
                {
                    $project: {
                        _id: 1,
                        row_id: 1,
                        comment: {
                            $switch: {
                                branches: branches,
                                default: null,
                            },
                        },
                        site: 1,
                        ip: 1,
                        israelStatus: 1,
                        ittihadStatus: 1,
                        pageData: {
                            $cond: {
                                if: { $eq: ['$site', 'israel-today'] },
                                then: '$pageData.israelPage',
                                else: '$pageData.ittihadPage',
                            },
                        },
                        createdAt: 1,
                        updatedAt: 1,
                        approvalDate: 1,
                        userId: 1,
                        pageId: 1,
                        likeCount: { $size: '$like' },
                        replyComments: {
                            $map: {
                                input: '$replyComments',
                                as: 'reply',
                                in: {
                                    $switch: {
                                        branches: replyBranches,
                                        default: null,
                                    },
                                },
                            },
                        },
                    },
                },
                {
                    $match: queryObject,
                },
            ];

            let [allCommentsData, totalComment, israelUsers, ittihadUsers, likeData] = await Promise.all([
                this.commentModel
                    .aggregate(pipeline)
                    .sort({ createdAt: -1 })
                    .skip(pagination.skip)
                    .limit(pagination.resultPerPage),
                this.commentModel.aggregate(pipeline),
                this.israelUserModel.find().select('name image loggedInViaGoogle').lean(),
                this.ittihadUserModel.find().select('name image loggedInViaGoogle').lean(),
                this.CommentLikeModel.find().lean(),
            ]);

            const users = [...israelUsers, ...ittihadUsers];

            // let obj = {};
            // if (userId) {
            //     obj['userId'] = userId;
            // } else {
            //     obj['ip'] = searchObject.ip;
            // }

            allCommentsData.forEach(async (comment: any) => {
                comment['totalReplay'] = 0;
                if (comment?.userId) {
                    let user = users.find((user) => user?._id?.toString() === comment?.userId?.toString());

                    if (user) {
                        comment.name = decrypt(user.name);
                        comment.image = user.loggedInViaGoogle ? user.image : undefined;
                    }
                }

                likeData.map((like: any) => {
                    if (like?.userId && request?.user?._id) {
                        if (
                            like?.commentId?.toString() === comment?._id?.toString() &&
                            like?.userId?.toString() === request?.user?._id?.toString()
                        )
                            return (comment['like'] = true);
                    } else if (like?.ip && searchObject.ip) {
                        if (like?.commentId?.toString() === comment?._id?.toString() && like.ip === searchObject.ip)
                            return (comment['like'] = true);
                    }
                });

                comment.replyComments.map(async (item: any) => {
                    if (item?.userId) {
                        let user = users.find((user) => user?._id?.toString() === item?.userId?.toString());
                        if (user) {
                            item['name'] = decrypt(user.name);
                            item.image = user?.loggedInViaGoogle ? user?.image : undefined;
                        }
                    }

                    if (item?.israelStatus === 'approved' || item?.ittihadStatus === 'approved')
                        comment['totalReplay'] += 1;

                    likeData.map((like: any) => {
                        if (like?.userId && request?.user?._id) {
                            if (
                                like?.commentId?.toString() === item?.id?.toString() &&
                                like?.userId?.toString() === request?.user?._id?.toString()
                            )
                                return (item['like'] = true);
                        } else if (like?.ip && searchObject.ip) {
                            if (like?.commentId?.toString() === item?.id?.toString() && like.ip === searchObject.ip)
                                return (item['like'] = true);
                        }
                    });
                });

                comment.replyComments.sort((a: any, b: any) => {
                    if (a === null && b === null) {
                        return 0;
                    } else if (a === null) {
                        return 1;
                    } else if (b === null) {
                        return -1;
                    } else {
                        return b?.createdAt - a?.createdAt;
                    }
                });
            });

            return {
                pageData,
                allCommentsData,
                totalComment: totalComment.length,
            };
        } catch (error) {
            console.log('error in get article Page', error);
            return error.message;
        }
    }
}
