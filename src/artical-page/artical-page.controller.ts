import { Body, Controller, Get, Param, Post, Query, Req, Res } from '@nestjs/common';
import { ArticalPageService } from './artical-page.service';
import { CustomError } from 'src/helpers/customError';
import { CreateSearchObjectDto } from 'src/common/search_object.dto';
import { Request, Response } from 'express';
import { returnMessage } from 'src/helpers/utils';
import { ApiTags } from '@nestjs/swagger';

@Controller('api/v1/artical-page')
@ApiTags('Article Page')
export class ArticalPageController {
    constructor(private readonly articleService: ArticalPageService) {}
    @Post('articalPage')
    async getPage(
        @Req() request: Request,
        @Query('pageId') pageId: any,
        @Res() response: Response,
        @Body() searchObject: CreateSearchObjectDto,
        @Query('userId') userId: any,
        @Query('site') site: any,
    ) {
        const pagedata = await this.articleService.getArticlePage(request, pageId, searchObject, userId, site);
        if (typeof pagedata === 'string' && returnMessage('tokenNotExistOnArticle') == pagedata)
            throw new CustomError({ success: false, message: pagedata }, 400);
        if (typeof pagedata === 'string') throw new CustomError({ success: false }, 400);
        return response.status(200).json({
            success: true,
            data: pagedata,
        });
    }
}
