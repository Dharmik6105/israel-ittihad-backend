import { Injectable } from '@nestjs/common';
import { Page } from './pages.schema';
import { InjectModel } from '@nestjs/mongoose';
import { CreatePageDto } from './dto/create-page.dto';
import { capitalizeFirstLetter, getKeywordType, paginationObject, returnMessage } from 'src/helpers/utils';
import mongoose, { Model } from 'mongoose';
import { updatePageDto } from './dto/update-page.dto';
import { CreateSearchObjectDto } from 'src/common/search_object.dto';
import { decrypt, decryptArrayOfObject, encrypt } from 'src/helpers/encrypt-decrypt';
import { Comments } from 'src/comments/schema/comment.schema';
import { HistoryLogsService } from 'src/history-logs/history-logs.service';
import { UserService } from 'src/user/user.service';
import { PageSettingDocument, page_setting } from 'src/page-setting/page-setting.schema';
import moment from 'moment-timezone';
import { RabbitmqService } from 'src/rabbitmq/rabbitmq.service';
import * as dotenv from 'dotenv';
dotenv.config();
@Injectable()
export class PagesService {
    constructor(
        @InjectModel(Page.name, 'SYSTEM_DB') private readonly pageModel: Model<any>,
        @InjectModel(Comments.name, 'SYSTEM_DB') private readonly commentsModel: Model<any>,
        @InjectModel(page_setting.name, 'SYSTEM_DB') private readonly pageSettingModel: Model<PageSettingDocument>,
        private historyLogsService: HistoryLogsService,
        private readonly userService: UserService,
        private readonly rabbitmqService: RabbitmqService,
    ) {}

    async createPage(createPageDto: CreatePageDto, request: any) {
        try {
            const permissions = request.user.permissions;
            if (!permissions.harmfulWords.write) {
                return returnMessage('permissionDenied');
            }

            if (request.user.site === 'israelBackOffice') {
                if (createPageDto.ittihadPage) delete createPageDto.ittihadPage;
                if (createPageDto.ittihadUrl) delete createPageDto.ittihadUrl;
                if (createPageDto.ittihadStatus) delete createPageDto.ittihadStatus;
                if (createPageDto.ittihadPageScript) delete createPageDto.ittihadPageScript;
                createPageDto.israelPage = request.body.pageName;
                createPageDto.isrealUrl = request.body.pageUrl;
                createPageDto.israelStatus = request.body.status;
                createPageDto.israelPageCreatedBy = request.user._id;
                createPageDto.israelPageUpdatedBy = request.user._id;
                if (createPageDto.israelStatus === 'active') {
                    createPageDto['israelPublishDate'] = new Date();
                    createPageDto['israelPublishBy'] = request.user._id;
                }
            } else if (request.user.site === 'ittihadBackOffice') {
                if (createPageDto.israelPage) delete createPageDto.israelPage;
                if (createPageDto.isrealUrl) delete createPageDto.isrealUrl;
                if (createPageDto.israelStatus) delete createPageDto.israelStatus;
                createPageDto.ittihadPage = request.body.pageName;
                createPageDto.ittihadUrl = request.body.pageUrl;
                createPageDto.ittihadStatus = request.body.status;
                createPageDto.ittihadPageCreatedBy = request.user._id;
                createPageDto.ittihadPageUpdatedBy = request.user._id;
                if (createPageDto.ittihadStatus === 'active') {
                    createPageDto['ittihadPublishDate'] = new Date();
                    createPageDto['ittihadPublishBy'] = request.user._id;
                }
            }

            const rowId = await this.pageModel.countDocuments();

            createPageDto['row_id'] = rowId + 1;

            let createPageData = await this.pageModel.create(createPageDto);

            if (createPageData) {
                if (
                    request.user.site === 'israelBackOffice' &&
                    request.body.status === 'active' &&
                    createPageData.israelStatus === 'active' &&
                    createPageData?.isrealUrl
                ) {
                    this.approvePageEmailNotification('ittihadBackOffice', createPageData?._id.toString());
                } else if (
                    request.user.site === 'ittihadBackOffice' &&
                    request.body.status === 'active' &&
                    createPageData.ittihadStatus === 'active' &&
                    createPageData?.ittihadUrl
                ) {
                    this.approvePageEmailNotification('israelBackOffice', createPageData?._id.toString());
                }
            }

            let historyLogsData = {
                logId: createPageData._id,
                method: 'Create',
                data: 'Page was created',
                site: request.user.site === 'israelBackOffice' ? 'israel-today' : 'ittihad-today',
                updatedBy: request.user._id,
                module: 'pages',
            };

            await this.historyLogsService.addHistoryLog(historyLogsData);

            this.newPageEmailNotification(request.user.site, createPageData);

            if (request.user.site === 'israelBackOffice' && createPageDto.israelStatus === 'active') {
                createPageData.israelPageScript = `<div name="article_page_id" id ="load-article-comment" for="israel-${createPageData._id}" ></div>
                <script type="module" src="${process.env.ARTICLE_PAGE_SCRIPT}"></script>
                `;
            } else if (request.user.site === 'ittihadBackOffice' && createPageDto.ittihadStatus === 'active') {
                createPageData.ittihadPageScript = `<div  name="article_page_id" id ="load-article-comment" for="ittihad-${createPageData._id}"></div>
                <script type="module"  src="${process.env.ARTICLE_PAGE_SCRIPT}"></script>`;
            }
            await createPageData.save();

            let totalPendings = await this.userService.pendingCounts({ site: 'israelBackOffice' });
            if (totalPendings)
                this.rabbitmqService.publish(`UpdatedCount:israelBackOffice`, JSON.stringify(totalPendings));

            totalPendings = await this.userService.pendingCounts({ site: 'ittihadBackOffice' });
            if (totalPendings)
                this.rabbitmqService.publish(`UpdatedCount:ittihadBackOffice`, JSON.stringify(totalPendings));
            return createPageData;
        } catch (error) {
            console.log('error in create Page', error);
            return error.message;
        }
    }

    async newPageEmailNotification(site: string, createPageDto: object): Promise<any> {
        const pageSetting = await this.pageSettingModel.findOne({ site }).lean();

        if (
            pageSetting.newpage_email_message === '' ||
            pageSetting.newpage_email_from === '' ||
            pageSetting.newpage_email_sub === '' ||
            pageSetting.newpage_email_reply === ''
        )
            return returnMessage('newPageEmailTemplateNotAvailable');

        // let pageUrl: string = createPageDto['isrealUrl'];

        // if (site === 'ittihadBackOffice' && createPageDto['ittihadUrl']) pageUrl = createPageDto['ittihadUrl'];

        pageSetting.newpage_email_message = pageSetting.newpage_email_message.replaceAll(
            '{{page_url}}',
            `${process.env.WEBHOST}/pages/edit/${createPageDto['_id']}`,
        );

        let emails = pageSetting.pages_notifications[0];
        JSON.parse(emails).map((email: string) => {
            this.userService.sendMail(
                pageSetting.newpage_email_from,
                email,
                pageSetting.newpage_email_reply,
                pageSetting.newpage_email_sub,
                pageSetting.newpage_email_message,
            );
        });

        return true;
    }

    async approvePageEmailNotification(site: string, pageId: string): Promise<any> {
        const pageSetting = await this.pageSettingModel.findOne({ site }).lean();

        if (
            pageSetting.newpage_email_message === '' ||
            pageSetting.newpage_email_from === '' ||
            pageSetting.newpage_email_sub === '' ||
            pageSetting.newpage_email_reply === ''
        )
            return returnMessage('newPageEmailTemplateNotAvailable');

        pageSetting.newpage_email_message = pageSetting.newpage_email_message.replaceAll(
            '{{page_url}}',
            `${process.env.WEBHOST}/pages/edit/${pageId}`,
        );

        let emails = pageSetting.pages_notifications[0];
        JSON.parse(emails).map((email: string) => {
            this.userService.sendMail(
                pageSetting.newpage_email_from,
                email,
                pageSetting.newpage_email_reply,
                pageSetting.newpage_email_sub,
                pageSetting.newpage_email_message,
            );
        });

        return true;
    }
    async updatepage(id: string, updatePageDto: updatePageDto, request: any): Promise<any> {
        try {
            const permissions = request.user.permissions;
            if (!permissions.pages.write) {
                return returnMessage('permissionDenied');
            }
            const page = await this.pageModel.findById(id);

            if (!page) return returnMessage('pageNotFound');
            let createHistoryLog = true;
            if (request.body && Object.keys(request.body).length === 0) createHistoryLog = false;
            if (request.user.site === 'israelBackOffice') {
                if (updatePageDto.ittihadPage) delete updatePageDto.ittihadPage;
                if (updatePageDto.ittihadUrl) delete updatePageDto.ittihadUrl;
                if (updatePageDto.ittihadStatus) delete updatePageDto.ittihadStatus;
                if (updatePageDto.israelPageScript) delete updatePageDto.israelPageScript;
                updatePageDto.israelPage = request.body.pageName;
                updatePageDto.isrealUrl = request.body.pageUrl;
                updatePageDto.israelStatus = request.body.status;
                updatePageDto.israelPageCreatedBy = request.user._id;
                updatePageDto.israelPageUpdatedBy = request.user._id;
                if (request.body.status === 'active' && page.israelStatus !== 'active') {
                    updatePageDto.israelPublishDate = new Date();
                    updatePageDto.israelPublishBy = request.user._id;
                } else if (request.body.status !== 'active' && request.body.status !== undefined) {
                    updatePageDto.israelPublishDate = null;
                    updatePageDto.israelPublishBy = null;
                    updatePageDto['israelPageScript'] = null;
                }
            } else if (request.user.site === 'ittihadBackOffice') {
                if (updatePageDto.israelPage) delete updatePageDto.israelPage;
                if (updatePageDto.isrealUrl) delete updatePageDto.isrealUrl;
                if (updatePageDto.israelStatus) delete updatePageDto.israelStatus;
                if (updatePageDto.ittihadPageScript) delete updatePageDto.ittihadPageScript;
                updatePageDto.ittihadPage = request.body.pageName;
                updatePageDto.ittihadUrl = request.body.pageUrl;
                updatePageDto.ittihadStatus = request.body.status;
                updatePageDto.ittihadPageCreatedBy = request.user._id;
                updatePageDto.ittihadPageUpdatedBy = request.user._id;
                if (request.body.status === 'active' && page.ittihadStatus !== 'active') {
                    updatePageDto.ittihadPublishDate = new Date();
                    updatePageDto.ittihadPublishBy = request.user._id;
                } else if (request.body.status !== 'active' && request.body.status !== undefined) {
                    updatePageDto.ittihadPublishDate = null;
                    updatePageDto.ittihadPublishBy = null;
                    updatePageDto['ittihadPageScript'] = null;
                }
            }

            if (
                request.user.site === 'israelBackOffice' &&
                request.body.status === 'active' &&
                page.israelStatus !== 'active'
            ) {
                updatePageDto.israelPageScript = `<div name="article_page_id" id ="load-article-comment" for="israel-${id}" ></div>
                <script type="module"  src="${process.env.ARTICLE_PAGE_SCRIPT}"></script>
                `;
            } else if (
                request.user.site === 'ittihadBackOffice' &&
                request.body.status === 'active' &&
                page.ittihadStatus !== 'active'
            ) {
                updatePageDto.ittihadPageScript = `<div  name="article_page_id" id ="load-article-comment" for="ittihad-${id}"></div>
                <script type="module"  src="${process.env.ARTICLE_PAGE_SCRIPT}"></script>`;
            }

            const updatedPage = await this.pageModel.findByIdAndUpdate(id, updatePageDto, { new: true });
            if (createHistoryLog) {
                if (
                    request.user.site === 'israelBackOffice' &&
                    request.body.status === 'active' &&
                    page.israelStatus !== 'active' &&
                    updatedPage?.isrealUrl
                ) {
                    this.approvePageEmailNotification('ittihadBackOffice', id);
                } else if (
                    request.user.site === 'ittihadBackOffice' &&
                    request.body.status === 'active' &&
                    page.ittihadStatus !== 'active' &&
                    updatedPage?.ittihadUrl
                ) {
                    this.approvePageEmailNotification('israelBackOffice', id);
                }
            }

            if (createHistoryLog) {
                let historyLogsData = {
                    logId: updatedPage._id,
                    method: 'Update',
                    data: request.body,
                    site: request.user.site === 'israelBackOffice' ? 'israel-today' : 'ittihad-today',
                    updatedBy: request.user._id,
                    module: 'pages',
                };

                await this.historyLogsService.addHistoryLog(historyLogsData);
            }

            let totalPendings = await this.userService.pendingCounts({ site: 'israelBackOffice' });
            if (totalPendings)
                this.rabbitmqService.publish(`UpdatedCount:israelBackOffice`, JSON.stringify(totalPendings));

            totalPendings = await this.userService.pendingCounts({ site: 'ittihadBackOffice' });
            if (totalPendings)
                this.rabbitmqService.publish(`UpdatedCount:ittihadBackOffice`, JSON.stringify(totalPendings));

            return updatedPage;
        } catch (error) {
            console.log('error in updating Page', error);
            return error.message;
        }
    }

    async pageList(searchObject: CreateSearchObjectDto, request: any) {
        try {
            const permissions = request.user.permissions;
            if (!permissions.pages.read) {
                return returnMessage('permissionDenied');
            }

            const pagination: any = paginationObject(searchObject);
            const queryConditions: any = {
                is_deleted: false,
                // site: request.user.site,
            };

            if (searchObject.search && searchObject.search !== '') {
                queryConditions['$or'] = [
                    { israelStatus: { $regex: searchObject.search.replace(/[^a-zA-Z0-9 ]/g, ''), $options: 'i' } },
                    { ittihadStatus: { $regex: searchObject.search.replace(/[^a-zA-Z0-9 ]/g, ''), $options: 'i' } },
                    { israelPage: { $regex: searchObject.search.replace(/[^a-zA-Z0-9 ]/g, ''), $options: 'i' } },
                    { ittihadPage: { $regex: searchObject.search.replace(/[^a-zA-Z0-9 ]/g, ''), $options: 'i' } },
                    { israelPublishDate: { $regex: searchObject.search.replace(/[^a-zA-Z0-9 ]/g, ''), $options: 'i' } },
                    {
                        ittihadPublishDate: {
                            $regex: searchObject.search.replace(/[^a-zA-Z0-9 ]/g, ''),
                            $options: 'i',
                        },
                    },
                    {
                        israelCommentCount: {
                            $regex: searchObject.search.replace(/[^a-zA-Z0-9 ]/g, ''),
                            $options: 'i',
                        },
                    },
                    {
                        ittihadCommentCount: {
                            $regex: searchObject.search.replace(/[^a-zA-Z0-9 ]/g, ''),
                            $options: 'i',
                        },
                    },
                    {
                        'israelUpdatedByAdmin.firstname': {
                            $regex: encrypt(searchObject.search.toLowerCase()),
                            $options: 'i',
                        },
                    },
                    {
                        'israelUpdatedByAdmin.lastname': {
                            $regex: encrypt(searchObject.search.toLowerCase()),
                            $options: 'i',
                        },
                    },
                    {
                        'israelUpdatedByAdmin.fullname': {
                            $regex: encrypt(searchObject.search.toLowerCase()),
                            $options: 'i',
                        },
                    },
                    {
                        'ittihadUpdatedByAdmin.firstname': {
                            $regex: encrypt(searchObject.search.toLowerCase()),
                            $options: 'i',
                        },
                    },
                    {
                        'ittihadUpdatedByAdmin.lastname': {
                            $regex: encrypt(searchObject.search.toLowerCase()),
                            $options: 'i',
                        },
                    },
                    {
                        'ittihadUpdatedByAdmin.fullname': {
                            $regex: encrypt(searchObject.search.toLowerCase()),
                            $options: 'i',
                        },
                    },
                ];

                const keywordType = getKeywordType(searchObject.search);
                if (keywordType === 'number') {
                    const numericKeyword = parseInt(searchObject.search);
                    queryConditions['$or'].push({ row_id: numericKeyword });
                    queryConditions['$or'].push({ isrealCommentsCount: numericKeyword });
                    queryConditions['$or'].push({ ittihadCommentCount: numericKeyword });
                } else if (keywordType === 'date') {
                    const dateKeyword = new Date(searchObject.search);
                    queryConditions['$or'].push({ israelPublishDate: dateKeyword });
                    queryConditions['$or'].push({ ittihadPublishDate: dateKeyword });
                }
            }

            const aggregationPipeline = [
                {
                    $lookup: {
                        from: 'admins',
                        localField: 'israelPublishBy',
                        foreignField: '_id',
                        as: 'israelUpdatedByAdmin',
                        pipeline: [{ $project: { firstname: 1, lastname: 1, fullname: 1 } }],
                    },
                },

                {
                    $lookup: {
                        from: 'admins',
                        localField: 'ittihadPublishBy',
                        foreignField: '_id',
                        as: 'ittihadUpdatedByAdmin',
                        pipeline: [{ $project: { firstname: 1, lastname: 1, fullname: 1 } }],
                    },
                },
                {
                    $match: queryConditions,
                },
                {
                    $project: {
                        row_id: 1,
                        israelStatus: 1,
                        is_deleted: 1,
                        ittihadStatus: 1,
                        israelPage: 1,
                        ittihadPage: 1,
                        ittihadCommentCount: 1,
                        isrealCommentsCount: 1,
                        israelPublishDate: 1,
                        ittihadPublishDate: 1,
                        israelPageCreatedBy: 1,
                        israelPageUpdatedBy: 1,
                        ittihadPageCreatedBy: 1,
                        ittihadPageUpdatedBy: 1,
                        israelPublishBy: 1,
                        ittihadPublishBy: 1,
                        fullname: 1,
                        israelUpdatedByAdmin: {
                            $cond: {
                                if: { $eq: [{ $size: '$israelUpdatedByAdmin' }, 0] },
                                then: '$$REMOVE',
                                else: { $arrayElemAt: ['$israelUpdatedByAdmin', 0] },
                            },
                        },
                        ittihadUpdatedByAdmin: {
                            $cond: {
                                if: { $eq: [{ $size: '$ittihadUpdatedByAdmin' }, 0] },
                                then: '$$REMOVE',
                                else: { $arrayElemAt: ['$ittihadUpdatedByAdmin', 0] },
                            },
                        },
                    },
                },
            ];

            const commentsPipeline = [
                {
                    $unionWith: {
                        coll: 'commentreplays',
                    },
                },
                {
                    $group: {
                        _id: {
                            pageId: '$pageId',
                            site: '$site',
                        },
                        commentCount: { $sum: 1 }, // Count comments for each page and site
                    },
                },
            ];

            const commentCounts = await this.commentsModel.aggregate(commentsPipeline);

            const pageData = await this.pageModel
                .aggregate(aggregationPipeline)
                .collation({ locale: 'en' })
                .sort(pagination.sort)
                .skip(pagination.skip)

                .limit(pagination.resultPerPage);

            // Combine comment counts with page data
            let pageDataWithCommentCounts = pageData.map((page) => {
                const israelCommentCount = commentCounts.find(
                    (cc) => cc?._id?.pageId?.equals(page._id) && cc?._id?.site === 'israel-today',
                );

                const ittihadCommentCount = commentCounts.find(
                    (cc) => cc?._id?.pageId?.equals(page._id) && cc?._id?.site === 'ittihad-today',
                );

                return {
                    ...page,
                    israelCommentCount: israelCommentCount ? israelCommentCount.commentCount : 0,
                    ittihadCommentCount: ittihadCommentCount ? ittihadCommentCount.commentCount : 0,
                };
            });

            if (pagination.sort.ittihadCommentCount) {
                if (pagination.sort.ittihadCommentCount >= 1) {
                    function sortAscendingPrice(a: any, b: any) {
                        return a.ittihadCommentCount - b.ittihadCommentCount;
                    }
                    pageDataWithCommentCounts.sort(sortAscendingPrice);
                } else {
                    function sortDescendingPrice(a: any, b: any) {
                        return b.ittihadCommentCount - a.ittihadCommentCount;
                    }
                    pageDataWithCommentCounts.sort(sortDescendingPrice);
                }
            }

            if (pagination.sort.israelCommentCount) {
                if (pagination.sort.israelCommentCount >= 1) {
                    function sortAscendingPrice(a: any, b: any) {
                        return a.israelCommentCount - b.israelCommentCount;
                    }
                    pageDataWithCommentCounts.sort(sortAscendingPrice);
                } else {
                    function sortDescendingPrice(a: any, b: any) {
                        return b.israelCommentCount - a.israelCommentCount;
                    }
                    pageDataWithCommentCounts.sort(sortDescendingPrice);
                }
            }

            if (!pageDataWithCommentCounts) return returnMessage('pageNotFound');

            const pageCount = await this.pageModel.aggregate(aggregationPipeline);

            return {
                pageCount: Math.ceil(pageCount.length / pagination.resultPerPage) || 0,
                pageData: decryptArrayOfObject(pageDataWithCommentCounts, [
                    'israelUpdatedByAdmin',
                    'ittihadUpdatedByAdmin',
                    'firstname',
                    'lastname',
                ]),
            };
        } catch (error) {
            console.log('error in getting  Page', error);
            return error.message;
        }
    }

    async getPageByID(id: string, request: any) {
        try {
            const permissions = request.user.permissions;
            if (!permissions.pages.read) {
                return returnMessage('permissionDenied');
            }
            const queryConditions: any = {
                _id: new mongoose.Types.ObjectId(id),
                is_deleted: false,
            };
            const aggregationPipeline = [
                {
                    $lookup: {
                        from: 'admins',
                        localField: 'israelPublishBy',
                        foreignField: '_id',
                        as: 'israelUpdatedByAdmin',
                        pipeline: [{ $project: { firstname: 1, lastname: 1 } }],
                    },
                },

                {
                    $lookup: {
                        from: 'admins',
                        localField: 'ittihadPublishBy',
                        foreignField: '_id',
                        as: 'ittihadUpdatedByAdmin',
                        pipeline: [{ $project: { firstname: 1, lastname: 1 } }],
                    },
                },

                {
                    $match: queryConditions,
                },
                {
                    $project: {
                        row_id: 1,
                        israelStatus: 1,
                        is_deleted: 1,
                        ittihadStatus: 1,
                        israelPage: 1,
                        ittihadPage: 1,
                        isrealComments: 1,
                        ittihadComments: 1,
                        israelPublishDate: 1,
                        ittihadPublishDate: 1,
                        israelPageCreatedBy: 1,
                        israelPageUpdatedBy: 1,
                        ittihadPageCreatedBy: 1,
                        ittihadPageUpdatedBy: 1,
                        ittihadPageScript: 1,
                        israelPageScript: 1,
                        isrealUrl: 1,
                        ittihadUrl: 1,
                        israelUpdatedByAdmin: {
                            $cond: {
                                if: { $eq: [{ $size: '$israelUpdatedByAdmin' }, 0] },
                                then: '$$REMOVE',
                                else: { $arrayElemAt: ['$israelUpdatedByAdmin', 0] },
                            },
                        },
                        ittihadUpdatedByAdmin: {
                            $cond: {
                                if: { $eq: [{ $size: '$ittihadUpdatedByAdmin' }, 0] },
                                then: '$$REMOVE',
                                else: { $arrayElemAt: ['$ittihadUpdatedByAdmin', 0] },
                            },
                        },
                    },
                },
            ];

            const existingPage = await this.pageModel.aggregate(aggregationPipeline);

            const commentsPipeline = [
                {
                    $unionWith: {
                        coll: 'commentreplays',
                    },
                },
                {
                    $group: {
                        _id: {
                            pageId: '$pageId',
                            site: '$site',
                        },
                        commentCount: { $sum: 1 }, // Count comments for each page and site
                    },
                },
            ];

            const commentCounts = await this.commentsModel.aggregate(commentsPipeline);

            // Combine comment counts with page data
            const pageDataWithCommentCounts = existingPage.map((page) => {
                const israelCommentCount = commentCounts.find(
                    (cc) => cc?._id?.pageId?.equals(page?._id) && cc?._id?.site === 'israel-today',
                );
                const ittihadCommentCount = commentCounts.find(
                    (cc) => cc?._id?.pageId?.equals(page?._id) && cc?._id?.site === 'ittihad-today',
                );
                return {
                    ...page,
                    israelCommentCount: israelCommentCount ? israelCommentCount.commentCount : 0,
                    ittihadCommentCount: ittihadCommentCount ? ittihadCommentCount.commentCount : 0,
                };
            });
            if (!pageDataWithCommentCounts) return returnMessage('pageNotFound');

            return {
                pageData: decryptArrayOfObject(pageDataWithCommentCounts, [
                    'israelUpdatedByAdmin',
                    'israelUpdatedByAdmin',
                    'ittihadUpdatedByAdmin',
                    'firstname',
                    'lastname',
                ]),
            };
        } catch (error) {
            console.log('error in get Page', error);
            return error.message;
        }
    }

    async downloadExcel(request: any): Promise<any> {
        try {
            const queryConditions: any = {
                is_deleted: false,
                // site: request.user.site,
            };

            const aggregationPipeline = [
                {
                    $lookup: {
                        from: 'admins',
                        localField: 'israelPublishBy',
                        foreignField: '_id',
                        as: 'israelUpdatedByAdmin',
                        pipeline: [{ $project: { firstname: 1, lastname: 1 } }],
                    },
                },

                {
                    $lookup: {
                        from: 'admins',
                        localField: 'ittihadPublishBy',
                        foreignField: '_id',
                        as: 'ittihadUpdatedByAdmin',
                        pipeline: [{ $project: { firstname: 1, lastname: 1 } }],
                    },
                },

                {
                    $match: queryConditions,
                },
                {
                    $project: {
                        _id: 0,
                        row_id: 1,
                        israelStatus: 1,
                        ittihadStatus: 1,
                        israelPage: 1,
                        ittihadPage: 1,
                        ittihadCommentCount: 1,
                        isrealCommentsCount: 1,
                        israelPublishDate: 1,
                        ittihadPublishDate: 1,
                        israelUpdatedByAdmin: {
                            $cond: {
                                if: { $eq: [{ $size: '$israelUpdatedByAdmin' }, 0] },
                                then: '$$REMOVE',
                                else: { $arrayElemAt: ['$israelUpdatedByAdmin', 0] },
                            },
                        },

                        ittihadUpdatedByAdmin: {
                            $cond: {
                                if: { $eq: [{ $size: '$ittihadUpdatedByAdmin' }, 0] },
                                then: '$$REMOVE',
                                else: { $arrayElemAt: ['$ittihadUpdatedByAdmin', 0] },
                            },
                        },
                    },
                },
            ];

            const commentsPipeline = [
                {
                    $group: {
                        _id: {
                            pageId: '$pageId',
                            site: '$site',
                        },
                        commentCount: { $sum: 1 }, // Count comments for each page and site
                    },
                },
            ];

            const commentCounts = await this.commentsModel.aggregate(commentsPipeline);

            const pageData = await this.pageModel.aggregate(aggregationPipeline);

            // Combine comment counts with page data
            let pageDataWithCommentCounts = pageData.map((page) => {
                const israelCommentCount = commentCounts.find(
                    (cc) => cc?._id?.pageId?.equals(page._id) && cc?._id?.site === 'israel-today',
                );

                const ittihadCommentCount = commentCounts.find(
                    (cc) => cc?._id?.pageId?.equals(page._id) && cc?._id?.site === 'ittihad-today',
                );

                return {
                    ...page,
                    israelCommentCount: israelCommentCount ? israelCommentCount.commentCount : 0,
                    ittihadCommentCount: ittihadCommentCount ? ittihadCommentCount.commentCount : 0,
                };
            });
            if (!pageDataWithCommentCounts) return returnMessage('pageNotFound');

            return this.optimiseExcel(pageDataWithCommentCounts);
        } catch (error) {
            console.log('error in getting page', error);
            return error.message;
        }
    }

    optimiseExcel(data: any[]): any {
        try {
            const batchSize = Math.ceil(Math.sqrt(data.length));
            let chunkArray: any[] = [];
            for (let index = 0; index < batchSize; index++) {
                chunkArray.push(data.splice(0, batchSize));
            }

            let updatedData = chunkArray.map((element: any) => {
                return this.optimisedPageXls(element);
            });
            return [].concat.apply([], updatedData);
        } catch (error) {
            console.log('error while exporting pages data', error);
            return error.message;
        }
    }

    optimisedPageXls(data: any[]): any {
        try {
            const newArrayData: any[] = [];
            data.forEach((element: any) => {
                let obj = {};
                obj['Id'] = element.row_id || '-';

                if (element.israelStatus) {
                    if (element.israelStatus === 'active') obj['Israel T status'] = 'Approved';
                    else if (element.israelStatus === 'pending') obj['Israel T status'] = 'Pending';
                    else if (element.israelStatus === 'notApproved') obj['Israel T status'] = 'Not-Approved';
                } else obj['Israel T status'] = '-';

                obj['Israel T page'] = element.israelPage || '-';
                obj['Israel T comments'] = element.israelCommentCount;
                obj['Israel T publish date'] = element.israelPublishDate
                    ? moment(element.israelPublishDate).tz('Israel').format('DD.MM.YYYY HH:mm')
                    : '-';

                if (element.israelUpdatedByAdmin) {
                    obj['Israel T publish date'] =
                        obj['Israel T publish date'] +
                        '\n' +
                        capitalizeFirstLetter(decrypt(element.israelUpdatedByAdmin.firstname)) +
                        ' ' +
                        capitalizeFirstLetter(decrypt(element.israelUpdatedByAdmin.lastname));
                } else if (element.israelUpdatedByUser) {
                    obj['Israel T publish date'] =
                        obj['Israel T publish date'] +
                        '\n' +
                        capitalizeFirstLetter(decrypt(element.israelUpdatedByUser.firstname)) +
                        ' ' +
                        capitalizeFirstLetter(decrypt(element.israelUpdatedByUser.lastname));
                }

                if (element.ittihadStatus) {
                    if (element.ittihadStatus === 'active') obj['Ittihad T status'] = 'Approved';
                    else if (element.ittihadStatus === 'pending') obj['Ittihad T status'] = 'In-Active';
                    else if (element.ittihadStatus === 'notApproved') obj['Ittihad T status'] = 'Not-Approved';
                } else obj['Ittihad T status'] = '-';

                obj['Ittihad T page'] = element.israelPage;
                obj['Ittihad T comments'] = element.ittihadCommentCount;
                obj['Ittihad T publish date'] = element.ittihadPublishDate
                    ? moment(element.ittihadPublishDate).tz('Israel').format('DD.MM.YYYY HH:mm')
                    : '-';

                if (element.ittihadUpdatedByAdmin) {
                    obj['Ittihad T publish date'] =
                        obj['Ittihad T publish date'] +
                        '\n' +
                        capitalizeFirstLetter(decrypt(element.ittihadUpdatedByAdmin.firstname)) +
                        ' ' +
                        capitalizeFirstLetter(decrypt(element.ittihadUpdatedByAdmin.lastname));
                } else if (element.ittihadUpdatedByUser) {
                    obj['Ittihad T publish date'] =
                        obj['Ittihad T publish date'] +
                        '\n' +
                        capitalizeFirstLetter(decrypt(element.ittihadUpdatedByUser.firstname)) +
                        ' ' +
                        capitalizeFirstLetter(decrypt(element.ittihadUpdatedByUser.lastname));
                }

                newArrayData.push(obj);
            });

            return newArrayData;
        } catch (error) {
            console.log('error while exporting pages data', error);
            return error.message;
        }
    }
}
