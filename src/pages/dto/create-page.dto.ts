import { ApiProperty } from '@nestjs/swagger';
import { IsDate, IsNotEmpty, IsNumber, IsString, MaxLength, isDate } from 'class-validator';

export class CreatePageDto {
    @IsNumber()
    @MaxLength(10)
    @IsNotEmpty()
    row_id: number;

    @ApiProperty({ default: 'pending' })
    @IsString()
    @MaxLength(30)
    @IsNotEmpty()
    israelStatus: string;

    @ApiProperty({ default: 'pending' })
    @IsString()
    @MaxLength(30)
    @IsNotEmpty()
    ittihadStatus: string;

    @ApiProperty()
    @IsString()
    @MaxLength(30)
    @IsNotEmpty()
    israelPage: string;

    @ApiProperty()
    @IsString()
    @MaxLength(30)
    @IsNotEmpty()
    ittihadPage: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    israelPageScript: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    ittihadPageScript: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    isrealUrl: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    ittihadUrl: string;

    @IsString()
    @MaxLength(30)
    @IsNotEmpty()
    israelPageCreatedBy;

    @IsString()
    @MaxLength(30)
    @IsNotEmpty()
    ittihadPublishBy;

    @IsString()
    @MaxLength(30)
    @IsNotEmpty()
    israelPublishBy;

    @IsString()
    @MaxLength(30)
    @IsNotEmpty()
    israelPageUpdatedBy;

    @IsString()
    @MaxLength(30)
    @IsNotEmpty()
    ittihadPageCreatedBy;

    @IsString()
    @MaxLength(30)
    @IsNotEmpty()
    ittihadPageUpdatedBy;

    @IsDate()
    israelPublishDate;

    @IsDate()
    ittihadPublishDate;
}
