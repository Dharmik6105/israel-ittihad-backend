import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongoSchema } from 'mongoose';

export type PageSettingDocument = page_setting & Document;
@Schema({ timestamps: true })
export class page_setting extends Document {
    @Prop({ required: true })
    pages_notifications: any[];

    @Prop({ required: true })
    comment_notifications: any[];

    @Prop({ type: String, required: true })
    top_banner_image: string;

    @Prop({ type: String, required: true })
    logo_image: string;

    @Prop({ type: String, required: true })
    login_image: string;

    @Prop({ type: String, required: true })
    top_title: string;

    @Prop({ type: String, required: true })
    sub_title: string;

    @Prop({ type: Boolean, default: false })
    mustLogin: boolean;

    @Prop({ type: String, required: true })
    footer_text: string;

    @Prop({ type: String, required: true })
    terms_privacy_policy: string;

    @Prop({ type: String, required: true })
    terms_privacy_policy_url: string;

    @Prop({ type: String, required: true })
    google_client_id: string;

    @Prop({ type: String, required: true })
    confirm_email_from: string;

    @Prop({ required: true })
    confirm_email_reply: string;

    @Prop({ required: true })
    confirm_email_sub: string;

    @Prop({ required: true })
    confirm_email_message: string;

    @Prop({ required: true })
    reset_email_from: string;

    @Prop({ required: true })
    reset_email_reply: string;

    @Prop({ required: true })
    reset_email_sub: string;

    @Prop({ required: true })
    reset_email_message: string;

    @Prop({ required: true })
    newpage_email_from: string;

    @Prop({ required: true })
    newpage_email_reply: string;

    @Prop({ required: true })
    newpage_email_sub: string;

    @Prop({ required: true })
    newpage_email_message: string;

    @Prop({ required: true })
    newcomment_email_from: string;

    @Prop({ required: true })
    newcomment_email_reply: string;

    @Prop({ required: true })
    newcomment_email_sub: string;

    @Prop({ required: true })
    newcomment_email_message: string;

    @Prop({ type: String })
    updatedBy: MongoSchema.Types.ObjectId;

    @Prop({ type: String })
    createdBy: MongoSchema.Types.ObjectId;

    @Prop({
        required: true,
        enum: ['israelBackOffice', 'ittihadBackOffice'],
    })
    site: string;

    @Prop({ type: String, required: true })
    confirmCommentPopUpMessage: string;

    @Prop({ type: String,required: true  })
    login_message: string;

    @Prop({ type: String ,required: true })
    registration_message: string;

    @Prop({ type: String ,required: true })
    reset_message: string;
}
export const SettingSchema = SchemaFactory.createForClass(page_setting);
