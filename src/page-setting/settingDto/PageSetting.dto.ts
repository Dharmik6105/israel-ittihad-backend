import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsBoolean, IsNotEmpty, IsNumber, IsString, MaxLength } from 'class-validator';

export class PageSettingDto {
    // @IsString()
    @ApiProperty({ required: true })
    @IsArray()
    pages_notifications: any[];

    @ApiProperty({ required: true })
    @IsArray()
    comment_notifications: any[];

    @ApiProperty({ required: true })
    @IsString()
    top_banner_image: string;

    @ApiProperty({ required: true })
    @IsString()
    logo_image: string;

    @ApiProperty({ required: true })
    @IsString()
    login_image: string;

    @ApiProperty({ required: true })
    @IsString()
    top_title: string;

    @ApiProperty({ required: true })
    @IsString()
    sub_title: string;

    @ApiProperty({ default: 'false', required: true })
    @IsBoolean()
    MustLogin: boolean;

    @ApiProperty({ required: true })
    @IsString()
    footer_text: string;

    @ApiProperty({ required: true })
    @IsString()
    terms_privacy_policy: string;

    @ApiProperty({ required: true })
    @IsString()
    terms_privacy_policy_url: string;

    @ApiProperty({ required: true })
    @IsString()
    google_client_id: string;

    @ApiProperty({ required: true })
    @IsString()
    confirm_email_from: string;

    @ApiProperty({ required: true })
    @IsString()
    confirm_email_reply: string;

    @ApiProperty({ required: true })
    @IsString()
    confirm_email_sub: string;

    @ApiProperty({ required: true })
    @IsString()
    confirm_email_message: string;

    @ApiProperty({ required: true })
    @IsString()
    reset_email_from: string;

    @ApiProperty({ required: true })
    @IsString()
    reset_email_reply: string;

    @ApiProperty({ required: true })
    @IsString()
    reset_email_sub: string;

    @ApiProperty({ required: true })
    @IsString()
    reset_email_message: string;

    @ApiProperty({ required: true })
    @IsString()
    newpage_email_from: string;

    @ApiProperty({ required: true })
    @IsString()
    newpage_email_reply: string;

    @ApiProperty({ required: true })
    @IsString()
    newpage_email_sub: string;

    @ApiProperty({ required: true })
    @IsString()
    newpage_email_message: string;

    @ApiProperty({ required: true })
    @IsString()
    newcomment_email_from: string;

    @ApiProperty({ required: true })
    @IsString()
    newcomment_email_reply: string;

    @ApiProperty({ required: true })
    @IsString()
    newcomment_email_sub: string;

    @ApiProperty({ required: true })
    @IsString()
    newcomment_email_message: string;

    @IsString()
    @MaxLength(30)
    @IsNotEmpty()
    updatedBy: string;

    @IsString()
    @MaxLength(30)
    @IsNotEmpty()
    createddBy: string;

    @ApiProperty({ default: 'israelBackOffice', required: true })
    @IsString()
    site: string;

    @ApiProperty({ required: true })
    @IsString()
    @IsNotEmpty()
    confirmCommentPopUpMessage: string;

    @ApiProperty({ required: true })
    @IsString()
    login_message: string;

    @ApiProperty({ required: true })
    @IsString()
    registration_message: string;

    @ApiProperty({ required: true })
    @IsString()
    reset_message: string;
}
