import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongoSchema, ObjectId } from 'mongoose';
import { Comments } from './comment.schema';

export type CommentReplayDocument = CommentReplay & Document;

@Schema({ timestamps: true })
export class CommentReplay extends Document {
    @Prop({ type: Number, required: true })
    row_id: number;

    @Prop({ type: MongoSchema.Types.ObjectId, ref: 'Comment' })
    commentId: Comments;

    @Prop({ type: MongoSchema.Types.ObjectId })
    userId: ObjectId;

    @Prop({ type: Array, default: [] })
    like: any[];

    @Prop({ type: String })
    ip: string;

    @Prop({ type: MongoSchema.Types.ObjectId })
    updatedByIttihad: ObjectId;

    @Prop({ type: MongoSchema.Types.ObjectId })
    updatedByIsrael: ObjectId;

    @Prop({ type: String, required: true })
    originalComment: string;

    @Prop({ type: String, default: null })
    updatedCommentByIsrael: string;

    @Prop({ type: String, default: null })
    updatedCommentByIttihad: string;

    @Prop({ type: String })
    israelTranslation: string;

    @Prop({ type: String })
    ittihadTranslation: string;

    @Prop({
        type: String,
        enum: ['israel-today', 'ittihad-today'],
        required: true,
    })
    site: string;

    @Prop({ type: Date })
    approvalDateForIsrael: Date;

    @Prop({ type: Date })
    approvalDateForIttihad: Date;

    @Prop({ type: String, enum: ['pending', 'approved', 'notApproved'], default: 'pending' })
    israelStatus: string;

    @Prop({ type: String, enum: ['pending', 'approved', 'notApproved'], default: 'pending' })
    ittihadStatus: string;

    @Prop({ type: MongoSchema.Types.ObjectId })
    pageId: ObjectId;

    @Prop({ type: MongoSchema.Types.ObjectId })
    approvedByIttihad: ObjectId;

    @Prop({ type: MongoSchema.Types.ObjectId })
    approvedByIsrael: ObjectId;
}

export const commentReplaySchema = SchemaFactory.createForClass(CommentReplay);
