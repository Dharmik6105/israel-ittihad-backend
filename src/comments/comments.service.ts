import mongoose, { Model } from 'mongoose';
import { decrypt, encrypt } from './../helpers/encrypt-decrypt';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Comments } from './schema/comment.schema';
import { IComment } from 'src/interfaces/comments.interface';
import { CreateCommentDto } from './dto/comment.dto';
import {
    capitalizeFirstLetter,
    getKeywordType,
    paginationObject,
    returnMessage,
    sanitizeComment,
    translateComment,
} from 'src/helpers/utils';
import { CreateCommentReplayDto } from './dto/commentReplay.dto';
import { CommentReplay } from './schema/commentReplay.schema';
import { ICommentRep } from 'src/interfaces/commentRep.interface';
import { CreateSearchObjectDto } from 'src/common/search_object.dto';
import { CommentLike } from './schema/commentsLike.schema';
import { decryptArrayOfObject } from 'src/helpers/encrypt-decrypt';
import { page_setting } from 'src/page-setting/page-setting.schema';
import { IPage } from 'src/page-setting/setting.interface';
import { HistoryLogsService } from 'src/history-logs/history-logs.service';
import { ArticlePageUser, ArticlePageUserDocument } from 'src/user/schema/article-page-user.schema';
import { Page, PageDocument } from 'src/pages/pages.schema';
import { RabbitmqService } from 'src/rabbitmq/rabbitmq.service';
import { UserService } from 'src/user/user.service';
import moment from 'moment-timezone';

@Injectable()
export class CommentsService {
    constructor(
        @InjectModel(Comments.name, 'SYSTEM_DB') private commentModel: Model<IComment>,
        @InjectModel(CommentLike.name, 'SYSTEM_DB') private CommentLikeModel: Model<IComment>,
        @InjectModel(Page.name, 'SYSTEM_DB') private pageModel: Model<PageDocument>,
        @InjectModel(CommentReplay.name, 'SYSTEM_DB') private commentReplayModel: Model<ICommentRep>,
        @InjectModel(ArticlePageUser.name, 'ISRAEL_DB') private israelUserModel: Model<ArticlePageUserDocument>,
        @InjectModel(ArticlePageUser.name, 'ITTIHAD_DB') private ittihadUserModel: Model<ArticlePageUserDocument>,
        @InjectModel(page_setting.name, 'SYSTEM_DB') private PageSettingModel: Model<IPage>,
        private historyLogsService: HistoryLogsService,
        private readonly rabbitmqService: RabbitmqService,
        private readonly userService: UserService,
    ) {}

    // create a new comment
    async createComment(commentsData: CreateCommentDto, request: any, pageId: string): Promise<any> {
        try {
            if (request.headers.authorization && request.headers.authorization.startsWith('Bearer')) {
                const token = request.headers.authorization.split(' ')[1];
                const user = await this.userService.verifyArticlePageAuthToken(token);
                if (typeof user === 'string') return user;
                request['user'] = user;
            }

            if (request?.user?._id) {
                const checkMaxComment = await this.checkForMaxComment({ id: request?.user?._id });
                if (checkMaxComment)
                    return returnMessage('commentRestriction', commentsData.site == 'israel-today' ? 'he' : 'ar');
            } else if (commentsData?.ip) {
                const checkMaxComment = await this.checkForMaxComment({ ip: commentsData?.ip });
                if (checkMaxComment)
                    return returnMessage('commentRestriction', commentsData.site == 'israel-today' ? 'he' : 'ar');
            }

            const pageSettings: any = await this.PageSettingModel.findOne({
                site: commentsData.site === 'israel-today' ? 'israelBackOffice' : 'ittihadBackOffice',
            }).lean();

            if (pageSettings && pageSettings.mustLogin === true) {
                if (!request.user || !request.user._id) {
                    return returnMessage('mustLogin', commentsData.site == 'israel-today' ? 'he' : 'ar');
                }
                if (!request.user.emailVerified)
                    return returnMessage('notVerified', commentsData.site == 'israel-today' ? 'he' : 'ar');
            }

            let pageExists = await this.pageModel.findById(pageId);

            if (!pageExists) return returnMessage('pageNotFound', commentsData.site == 'israel-today' ? 'he' : 'ar');
            if (
                (pageExists.israelStatus !== 'active' && commentsData.site === 'israel-today') ||
                (pageExists.ittihadStatus !== 'active' && commentsData.site === 'ittihad-today')
            )
                return returnMessage('pageNotApproved', commentsData.site == 'israel-today' ? 'he' : 'ar');

            const commentSanitize = await sanitizeComment(commentsData.originalComment);
            if (commentSanitize)
                return returnMessage('commentSanitize', commentsData.site == 'israel-today' ? 'he' : 'ar');

            commentsData.pageId = pageId;

            const [totalComments, totalReplyComments] = await Promise.all([
                this.commentModel.countDocuments(),
                this.commentReplayModel.countDocuments(),
            ]);

            commentsData['row_id'] = totalComments + totalReplyComments + 1;

            if (request?.user?._id) {
                commentsData.site = request?.user?.site;
                commentsData.userId = request?.user?._id;
            }

            // if (commentsData.site === 'israel-today') {
            let translate = await translateComment(commentsData.originalComment, 'ar');
            if (!translate.success) return returnMessage('default', commentsData.site == 'israel-today' ? 'he' : 'ar');
            commentsData['israelTranslation'] = translate.message;

            translate = await translateComment(commentsData.originalComment, 'iw');
            if (!translate.success) return returnMessage('default', commentsData.site == 'israel-today' ? 'he' : 'ar');
            commentsData['ittihadTranslation'] = translate.message;
            // } else if (commentsData.site === 'ittihad-today') {
            //     let translate = await translateComment(commentsData.originalComment, 'iw');
            //     if (!translate.success) return returnMessage('default');
            //     commentsData['ittihadTranslation'] = translate.message;

            //     translate = await translateComment(commentsData.originalComment, 'ar');
            //     if (!translate.success) return returnMessage('default');
            //     commentsData['israelTranslation'] = translate.message;
            // }

            const newComment = await this.commentModel.create(commentsData);

            let historyLogsData = {
                logId: newComment._id,
                method: 'Create',
                data: 'Comment was created',
                site: newComment.site,
                module: 'comments',
            };

            await this.historyLogsService.addHistoryLog(historyLogsData);

            let totalPendings = await this.userService.pendingCounts({ site: 'israelBackOffice' });
            if (totalPendings)
                this.rabbitmqService.publish(`UpdatedCount:israelBackOffice`, JSON.stringify(totalPendings));

            totalPendings = await this.userService.pendingCounts({ site: 'ittihadBackOffice' });
            if (totalPendings)
                this.rabbitmqService.publish(`UpdatedCount:ittihadBackOffice`, JSON.stringify(totalPendings));
            return { message: pageSettings.confirmCommentPopUpMessage };
        } catch (error) {
            console.log('error while add comment', error);
            return error.message;
        }
    }

    // add comment replay
    async createCommentReplay(commentId: string, commentsData: CreateCommentReplayDto, request: any): Promise<any> {
        try {
            if (request.headers.authorization && request.headers.authorization.startsWith('Bearer')) {
                const token = request.headers.authorization.split(' ')[1];
                const user = await this.userService.verifyArticlePageAuthToken(token);
                if (typeof user === 'string') return user;

                request['user'] = user;
            }

            if (request?.user?._id) {
                const checkMaxComment = await this.checkForMaxComment({ id: request?.user?._id });
                if (checkMaxComment)
                    return returnMessage('commentRestriction', commentsData.site == 'israel-today' ? 'he' : 'ar');
            } else if (commentsData?.ip) {
                const checkMaxComment = await this.checkForMaxComment({ ip: commentsData?.ip });
                if (checkMaxComment)
                    return returnMessage('commentRestriction', commentsData.site == 'israel-today' ? 'he' : 'ar');
            }

            const pageSettings: any = await this.PageSettingModel.findOne({
                site: commentsData.site === 'israel-today' ? 'israelBackOffice' : 'ittihadBackOffice',
            }).lean();

            if (pageSettings && pageSettings.mustLogin === true) {
                if (!request.user || !request.user._id) {
                    return returnMessage('mustLogin', commentsData.site == 'israel-today' ? 'he' : 'ar');
                }
                if (!request.user.emailVerified)
                    return returnMessage('notVerified', commentsData.site == 'israel-today' ? 'he' : 'ar');
            }
            //find comment
            const [commentExist, totalComments, totalReplyComments]: any = await Promise.all([
                this.commentModel.findById(commentId).lean(),
                this.commentModel.countDocuments().lean(),
                this.commentReplayModel.countDocuments().lean(),
            ]);
            if (!commentExist)
                return returnMessage('commentNotExist', commentsData.site == 'israel-today' ? 'he' : 'ar');

            const commentSanitize = await sanitizeComment(commentsData.commentReplay);
            if (commentSanitize)
                return returnMessage('commentSanitize', commentsData.site == 'israel-today' ? 'he' : 'ar');

            //add replay.commentReplay
            commentsData.commentId = commentId;
            commentsData['originalComment'] = commentsData.commentReplay;
            commentsData['site'] = commentsData.site;
            commentsData['pageId'] = commentExist.pageId;
            commentsData['row_id'] = totalComments + totalReplyComments + 1;
            if (request.user?._id) {
                commentsData.userId = request.user._id;
            } else {
                commentsData.ip = commentsData.ip;
            }

            // if (commentsData.site === 'israel-today') {
            let translate = await translateComment(commentsData['originalComment'], 'ar');
            if (!translate.success) return returnMessage('default');
            commentsData['israelTranslation'] = translate.message;
            // } else if (commentsData.site === 'ittihad-today') {
            translate = await translateComment(commentsData['originalComment'], 'iw');
            if (!translate.success) return returnMessage('default', commentsData.site == 'israel-today' ? 'he' : 'ar');
            commentsData['ittihadTranslation'] = translate.message;
            // }

            const replyComment = await this.commentReplayModel.create(commentsData);

            let historyLogsData = {
                logId: replyComment._id,
                method: 'Create',
                data: 'Comment was created',
                site: replyComment['site'],
                module: 'comments',
            };

            await this.historyLogsService.addHistoryLog(historyLogsData);

            // get array from comments.commentReplay
            let array = commentExist.replyComments;
            array.unshift(replyComment._id);
            await this.commentModel.findByIdAndUpdate(commentExist._id, commentExist);
            // const site = commentExist.site === 'israel-today' ? 'israelBackOffice' : 'ittihadBackOffice';

            let totalPendings = await this.userService.pendingCounts({ site: 'israelBackOffice' });
            if (totalPendings)
                this.rabbitmqService.publish(`UpdatedCount:israelBackOffice`, JSON.stringify(totalPendings));

            totalPendings = await this.userService.pendingCounts({ site: 'ittihadBackOffice' });
            if (totalPendings)
                this.rabbitmqService.publish(`UpdatedCount:ittihadBackOffice`, JSON.stringify(totalPendings));
            //update the comment replay array
            return { message: pageSettings.confirmCommentPopUpMessage };
        } catch (error) {
            console.log('error while replay the comment', error);
            return error.message;
        }
    }

    // get all comments for the article page
    async getAllCommentsData(searchObject: CreateSearchObjectDto, request: any, query: any): Promise<any> {
        try {
            const permissions = request.user.permissions;
            if (!permissions.systemAdmins.read) {
                return returnMessage('permissionDenied');
            }

            let pagination: any = paginationObject(searchObject);
            let queryObject = {
                $or: [{ israelStatus: 'approved' }, { ittihadStatus: 'approved' }],
            };

            if (pagination.sort.updatedBy) {
                pagination['sort'] = {
                    'updatedByIttihadAdmin.firstname': pagination.sort.updatedByIttihadAdmin,
                    'updatedByIsraelAdmin.firstname': pagination.sort.updatedByIsraelAdmin,
                };
            }

            if (pagination.sort.pageData) {
                pagination['sort'] = {
                    pageData: pagination.sort.pageData,
                };
            }

            if (query.pageId) {
                queryObject['pageId'] = new mongoose.Types.ObjectId(query.pageId);
            }

            if (query.userId) {
                queryObject['userId'] = new mongoose.Types.ObjectId(query.userId);
            }

            if (searchObject.search && searchObject.search !== '') {
                const searchArray: any = [
                    { israelStatus: { $regex: searchObject.search.replace(/[^a-zA-Z0-9 ]/g, ''), $options: 'i' } },
                    { ittihadStatus: { $regex: searchObject.search.replace(/[^a-zA-Z0-9 ]/g, ''), $options: 'i' } },
                    { originalComment: { $regex: searchObject.search.replace(/[^a-zA-Z0-9 ]/g, ''), $options: 'i' } },
                    { pageData: { $regex: searchObject.search.replace(/[^a-zA-Z0-9 ]/g, ''), $options: 'i' } },
                    {
                        'updatedByIsraelAdmin.firstname': {
                            $regex: encrypt(searchObject.search.toLocaleLowerCase()),
                            $options: 'i',
                        },
                    },
                    {
                        'updatedByIsraelAdmin.lastname': {
                            $regex: encrypt(searchObject.search.toLocaleLowerCase()),
                            $options: 'i',
                        },
                    },
                    {
                        'updatedByIsraelAdmin.fullname': {
                            $regex: encrypt(searchObject.search.toLowerCase()),
                            $options: 'i',
                        },
                    },
                    {
                        'updatedByIttihadAdmin.firstname': {
                            $regex: encrypt(searchObject.search.toLocaleLowerCase()),
                            $options: 'i',
                        },
                    },
                    {
                        'updatedByIttihadAdmin.lastname': {
                            $regex: encrypt(searchObject.search.toLocaleLowerCase()),
                            $options: 'i',
                        },
                    },
                    {
                        'updatedByIttihadAdmin.fullname': {
                            $regex: encrypt(searchObject.search.toLowerCase()),
                            $options: 'i',
                        },
                    },
                ];

                const keywordType = getKeywordType(searchObject.search);
                if (keywordType === 'number') {
                    const numericKeyword = parseInt(searchObject.search);
                    searchArray.push({ row_id: numericKeyword });
                } else if (keywordType === 'date') {
                    const dateKeyword = new Date(searchObject.search);
                    searchArray.push({ updatedAt: dateKeyword });
                    searchArray.push({ createdAt: dateKeyword });
                }
                queryObject['$or'] = [...queryObject['$or'], searchArray];
            }

            const pipeline = [
                {
                    $lookup: {
                        from: 'commentreplays',
                        localField: 'replyComments',
                        foreignField: '_id',
                        as: 'replyComments',
                    },
                },
                {
                    $lookup: {
                        from: 'admins',
                        localField: 'updatedByIsrael',
                        foreignField: '_id',
                        as: 'updatedByIsraelAdmin',
                        pipeline: [{ $project: { firstname: 1, lastname: 1, fullname: 1 } }],
                    },
                },
                {
                    $lookup: {
                        from: 'admins',
                        localField: 'updatedByIttihad',
                        foreignField: '_id',
                        as: 'updatedByIttihadAdmin',
                        pipeline: [{ $project: { firstname: 1, lastname: 1, fullname: 1 } }],
                    },
                },
                {
                    $lookup: {
                        from: 'pages',
                        localField: 'pageId',
                        foreignField: '_id',
                        as: 'pageData',
                        pipeline: [{ $project: { israelPage: 1, ittihadPage: 1 } }],
                    },
                },
                { $unwind: '$pageData' },
                {
                    $project: {
                        _id: 1,
                        row_id: 1,
                        originalComment: 1,
                        updatedCommentByIsrael: 1,
                        updatedCommentByIttihad: 1,
                        site: 1,
                        status: 1,
                        fullname: 1,
                        pageData: {
                            $cond: {
                                if: { $eq: ['$site', 'israel-today'] },
                                then: '$pageData.israelPage',
                                else: '$pageData.ittihadPage',
                            },
                        },
                        createdAt: 1,
                        updatedAt: 1,
                        approvalDate: 1,
                        userId: 1,
                        pageId: 1,
                        like: 1,
                        likeCount: { $size: '$like' },
                        replyComments: {
                            $map: {
                                input: '$replyComments',
                                as: 'reply',
                                in: {
                                    commentReplay: '$$reply.commentReplay',
                                    likeCount: { $size: '$$reply.like' },
                                    like: '$$reply.like',
                                    _id: '$$reply._id',
                                },
                            },
                        },
                        totalReplay: { $size: '$replyComments' },
                        updatedByIsraelAdmin: {
                            $cond: {
                                if: { $eq: [{ $size: '$updatedByIsraelAdmin' }, 0] },
                                then: '$$REMOVE',
                                else: { $arrayElemAt: ['$updatedByIsraelAdmin', 0] },
                            },
                        },
                        updatedByIttihadAdmin: {
                            $cond: {
                                if: { $eq: [{ $size: '$updatedByIttihadAdmin' }, 0] },
                                then: '$$REMOVE',
                                else: { $arrayElemAt: ['$updatedByIttihadAdmin', 0] },
                            },
                        },
                    },
                },
                {
                    $match: queryObject,
                },
            ];

            let allCommentsData = await this.commentModel.aggregate(pipeline).sort(pagination.sort);

            const [israelUsers, ittihadUsers] = await Promise.all([
                this.israelUserModel.find().select('name').lean(),
                this.ittihadUserModel.find().select('name').lean(),
            ]);

            const users = [...israelUsers, ...ittihadUsers];

            allCommentsData.map((comment: any) => {
                if (comment.userId) {
                    let user = users.find((user) => user._id.toString() === comment.userId.toString());
                    if (user) {
                        comment.name = decrypt(user.name);
                    }
                } else {
                    comment.name = 'Anonymous';
                }
            });

            allCommentsData = decryptArrayOfObject(allCommentsData, [
                'firstname',
                'lastname',
                'updatedByIsraelAdmin',
                'updatedByIttihadAdmin',
            ]);

            if (searchObject.sortField === 'name') {
                if (searchObject.sortOrder === 'asc') {
                    allCommentsData = allCommentsData.sort((a, b) => a.name.localeCompare(b.name));
                } else {
                    allCommentsData = allCommentsData.sort((a, b) => b.name.localeCompare(a.name));
                }
            }
            const page = pagination.page;
            const pageSize = pagination.resultPerPage;
            const startIndex = (page - 1) * pageSize;
            const endIndex = startIndex + pageSize;

            return {
                allCommentsData: allCommentsData.slice(startIndex, endIndex),
                pageCount: Math.ceil(allCommentsData.length / pagination.resultPerPage) || 0,
            };
        } catch (error) {
            console.log(error, 'error while loading comments');
            return error.message;
        }
    }

    async getAllCommentsDataForAdmin(searchObject: CreateSearchObjectDto, request: any, query: any): Promise<any> {
        try {
            const permissions = request.user.permissions;
            if (!permissions.systemAdmins.read) {
                return returnMessage('permissionDenied');
            }

            const branches = [
                {
                    case: {
                        $and: [
                            { $eq: [request?.user?.site, 'israelBackOffice'] },
                            { $eq: ['$site', 'israel-today'] },
                            {
                                $ne: [{ $ifNull: ['$updatedCommentByIsrael', 'undefined'] }, 'undefined'],
                            },
                        ],
                    },
                    then: {
                        dd: 259,
                        comment: '$updatedCommentByIsrael',
                    },
                },
                {
                    case: {
                        $and: [
                            { $eq: [request?.user?.site, 'israelBackOffice'] },
                            { $eq: ['$site', 'israel-today'] },
                            {
                                $eq: [{ $ifNull: ['$updatedCommentByIsrael', 'undefined'] }, 'undefined'],
                            },
                        ],
                    },
                    then: {
                        dd: 274,
                        comment: '$originalComment',
                    },
                },
                {
                    case: {
                        $and: [
                            { $eq: [request?.user?.site, 'ittihadBackOffice'] },
                            { $eq: ['$site', 'ittihad-today'] },
                            {
                                $ne: [{ $ifNull: ['$updatedCommentByIttihad', 'undefined'] }, 'undefined'],
                            },
                        ],
                    },
                    then: {
                        dd: 289,
                        comment: '$updatedCommentByIttihad',
                    },
                },
                {
                    case: {
                        $and: [
                            { $eq: [request?.user?.site, 'ittihadBackOffice'] },
                            { $eq: ['$site', 'ittihad-today'] },
                            {
                                $eq: [{ $ifNull: ['$updatedCommentByIttihad', 'undefined'] }, 'undefined'],
                            },
                        ],
                    },
                    then: {
                        dd: 304,
                        comment: '$originalComment',
                    },
                },
                {
                    case: {
                        $and: [
                            { $eq: [request?.user?.site, 'israelBackOffice'] },
                            { $eq: ['$site', 'ittihad-today'] },
                            {
                                $ne: [{ $ifNull: ['$updatedCommentByIsrael', 'undefined'] }, 'undefined'],
                            },
                            {
                                $ne: [{ $ifNull: ['$updatedCommentByIttihad', 'undefined'] }, 'undefined'],
                            },
                        ],
                    },
                    then: {
                        dd: 322,
                        comment: '$updatedCommentByIsrael',
                    },
                },
                {
                    case: {
                        $and: [
                            { $eq: [request?.user?.site, 'israelBackOffice'] },
                            { $eq: ['$site', 'ittihad-today'] },
                            {
                                $ne: [{ $ifNull: ['$updatedCommentByIsrael', 'undefined'] }, 'undefined'],
                            },
                            {
                                $eq: [{ $ifNull: ['$updatedCommentByIttihad', 'undefined'] }, 'undefined'],
                            },
                        ],
                    },
                    then: {
                        dd: 341,
                        comment: '$updatedCommentByIsrael',
                    },
                },
                {
                    case: {
                        $and: [
                            { $eq: [request?.user?.site, 'israelBackOffice'] },
                            { $eq: ['$site', 'ittihad-today'] },
                            {
                                $eq: [{ $ifNull: ['$updatedCommentByIsrael', 'undefined'] }, 'undefined'],
                            },
                            {
                                $ne: [{ $ifNull: ['$updatedCommentByIttihad', 'undefined'] }, 'undefined'],
                            },
                        ],
                    },
                    then: {
                        dd: 360,
                        comment: '$ittihadTranslation',
                    },
                },
                {
                    case: {
                        $and: [
                            { $eq: [request?.user?.site, 'israelBackOffice'] },
                            { $eq: ['$site', 'ittihad-today'] },
                            {
                                $eq: [{ $ifNull: ['$updatedCommentByIsrael', 'undefined'] }, 'undefined'],
                            },
                            {
                                $eq: [{ $ifNull: ['$updatedCommentByIttihad', 'undefined'] }, 'undefined'],
                            },
                        ],
                    },
                    then: {
                        dd: 379,
                        comment: '$ittihadTranslation',
                    },
                },
                {
                    case: {
                        $and: [
                            { $eq: [request?.user?.site, 'ittihadBackOffice'] },
                            { $eq: ['$site', 'israel-today'] },
                            {
                                $ne: [{ $ifNull: ['$updatedCommentByIsrael', 'undefined'] }, 'undefined'],
                            },
                            {
                                $ne: [{ $ifNull: ['$updatedCommentByIttihad', 'undefined'] }, 'undefined'],
                            },
                        ],
                    },
                    then: {
                        dd: 398,
                        comment: '$updatedCommentByIttihad',
                    },
                },
                {
                    case: {
                        $and: [
                            { $eq: [request?.user?.site, 'ittihadBackOffice'] },
                            { $eq: ['$site', 'israel-today'] },
                            {
                                $ne: [{ $ifNull: ['$updatedCommentByIttihad', 'undefined'] }, 'undefined'],
                            },
                            {
                                $eq: [{ $ifNull: ['$updatedCommentByIsrael', 'undefined'] }, 'undefined'],
                            },
                        ],
                    },
                    then: {
                        dd: 417,
                        comment: '$updatedCommentByIttihad',
                    },
                },
                {
                    case: {
                        $and: [
                            { $eq: [request?.user?.site, 'ittihadBackOffice'] },
                            { $eq: ['$site', 'israel-today'] },
                            {
                                $eq: [{ $ifNull: ['$updatedCommentByIttihad', 'undefined'] }, 'undefined'],
                            },
                            {
                                $ne: [{ $ifNull: ['$updatedCommentByIsrael', 'undefined'] }, 'undefined'],
                            },
                        ],
                    },
                    then: {
                        dd: 436,
                        comment: '$israelTranslation',
                    },
                },
                {
                    case: {
                        $and: [
                            { $eq: [request?.user?.site, 'ittihadBackOffice'] },
                            { $eq: ['$site', 'israel-today'] },
                            {
                                $eq: [{ $ifNull: ['$updatedCommentByIsrael', 'undefined'] }, 'undefined'],
                            },
                            {
                                $eq: [{ $ifNull: ['$updatedCommentByIttihad', 'undefined'] }, 'undefined'],
                            },
                        ],
                    },
                    then: {
                        dd: 455,
                        comment: '$israelTranslation',
                    },
                },
            ];
            let queryObject = {};
            let pagination: any = paginationObject(searchObject);

            if (pagination.sort.updatedBy) {
                pagination['sort'] = {
                    'updatedByIsraelAdmin.firstname': pagination.sort.updatedByIsraelAdmin,
                    'updatedByIttihadAdmin.firstname': pagination.sort.updatedByIttihadAdmin,
                };
            }

            if (pagination.sort.originalComment) {
                pagination['sort'] = {
                    'comment.comment': pagination.sort.originalComment,
                };
            }

            if (pagination.sort.pageData) {
                pagination['sort'] = {
                    pageData: pagination.sort.pageData,
                };
            }

            if (query.pageId) {
                queryObject['pageId'] = new mongoose.Types.ObjectId(query.pageId);
            }

            if (query.userId) {
                queryObject['userId'] = new mongoose.Types.ObjectId(query.userId);
            }

            if (searchObject.search && searchObject.search !== '') {
                const usersNameSearch = {
                    name: {
                        $regex: encrypt(searchObject.search.toLowerCase()),
                        $options: 'i',
                    },
                };
                const [israelUserId, ittihadUsersId] = await Promise.all([
                    this.israelUserModel.distinct('_id', usersNameSearch),
                    this.ittihadUserModel.distinct('_id', usersNameSearch),
                ]);

                const usersId = [...israelUserId, ...ittihadUsersId];

                queryObject['$or'] = [
                    { userId: { $in: usersId } },
                    { status: { $regex: searchObject.search.replace(/[^a-zA-Z0-9 ]/g, ''), $options: 'i' } },
                    { 'comment.comment': { $regex: searchObject.search.replace(/[^a-zA-Z0-9 ]/g, ''), $options: 'i' } },
                    {
                        'comment.comment': {
                            $regex: searchObject.search.replace(
                                /[\u0600-\u06FF\u0750-\u077F\u08A0-\u08FF\uFB50-\uFDFF\uFE70-\uFEFF]/g,
                                '',
                            ),
                            $options: 'i',
                        },
                    },
                    {
                        'comment.comment': {
                            $regex: searchObject.search.replace(/[\u0591-\u05F4\uFB1D-\uFB4F]/g, ''),
                            $options: 'i',
                        },
                    },
                    { pageData: { $regex: searchObject.search.replace(/[^a-zA-Z0-9 ]/g, ''), $options: 'i' } },
                    {
                        'approvedByIsrael.firstname': {
                            $regex: encrypt(searchObject.search.toLocaleLowerCase()),
                            $options: 'i',
                        },
                    },
                    {
                        'approvedByIsrael.lastname': {
                            $regex: encrypt(searchObject.search.toLocaleLowerCase()),
                            $options: 'i',
                        },
                    },
                    {
                        'approvedByIsrael.fullname': {
                            $regex: encrypt(searchObject.search.toLowerCase()),
                            $options: 'i',
                        },
                    },
                    {
                        'approvedByIttihad.firstname': {
                            $regex: encrypt(searchObject.search.toLocaleLowerCase()),
                            $options: 'i',
                        },
                    },
                    {
                        'approvedByIttihad.lastname': {
                            $regex: encrypt(searchObject.search.toLocaleLowerCase()),
                            $options: 'i',
                        },
                    },
                    {
                        'approvedByIttihad.fullname': {
                            $regex: encrypt(searchObject.search.toLowerCase()),
                            $options: 'i',
                        },
                    },
                ];

                const keywordType = getKeywordType(searchObject.search);
                if (keywordType === 'number') {
                    const numericKeyword = parseInt(searchObject.search);
                    queryObject['$or'].push({ row_id: numericKeyword });
                } else if (keywordType === 'date') {
                    const dateKeyword = new Date(searchObject.search);
                    queryObject['$or'].push({ updatedAt: dateKeyword });
                    queryObject['$or'].push({ createdAt: dateKeyword });
                }
            }

            const pipeline = [
                {
                    $unionWith: {
                        coll: 'commentreplays',
                    },
                },
                {
                    $lookup: {
                        from: 'commentreplays',
                        localField: 'replyComments',
                        foreignField: '_id',
                        as: 'replyComments',
                    },
                },
                {
                    $lookup: {
                        from: 'admins',
                        localField: 'updatedByIsrael',
                        foreignField: '_id',
                        as: 'updatedByIsraelAdmin',
                        pipeline: [{ $project: { firstname: 1, lastname: 1, fullname: 1 } }],
                    },
                },
                {
                    $lookup: {
                        from: 'admins',
                        localField: 'updatedByIttihad',
                        foreignField: '_id',
                        as: 'updatedByIttihadAdmin',
                        pipeline: [{ $project: { firstname: 1, lastname: 1, fullname: 1 } }],
                    },
                },
                {
                    $lookup: {
                        from: 'admins',
                        localField: 'approvedByIttihad',
                        foreignField: '_id',
                        as: 'approvedByIttihad',
                        pipeline: [{ $project: { firstname: 1, lastname: 1, fullname: 1 } }],
                    },
                },
                {
                    $lookup: {
                        from: 'admins',
                        localField: 'approvedByIsrael',
                        foreignField: '_id',
                        as: 'approvedByIsrael',
                        pipeline: [{ $project: { firstname: 1, lastname: 1, fullname: 1 } }],
                    },
                },
                {
                    $lookup: {
                        from: 'pages',
                        localField: 'pageId',
                        foreignField: '_id',
                        as: 'pageData',
                        pipeline: [{ $project: { israelPage: 1, ittihadPage: 1 } }],
                    },
                },
                { $unwind: '$pageData' },
                {
                    $project: {
                        _id: 1,
                        row_id: 1,
                        updatedCommentByIsrael: 1,
                        updatedCommentByIttihad: 1,
                        ittihadTranslation: 1,
                        israelTranslation: 1,
                        site: 1,
                        comment: {
                            $switch: {
                                branches: branches,
                                default: null,
                            },
                        },
                        status: {
                            $cond: {
                                if: { $eq: [request?.user?.site, 'israelBackOffice'] },
                                then: '$israelStatus',
                                else: '$ittihadStatus',
                            },
                        },
                        fullname: 1,
                        pageData: {
                            $cond: {
                                if: { $eq: ['$site', 'israel-today'] },
                                then: '$pageData.israelPage',
                                else: '$pageData.ittihadPage',
                            },
                        },
                        createdAt: 1,
                        updatedAt: 1,
                        approvalDate: {
                            $cond: {
                                if: { $eq: [request?.user?.site, 'israelBackOffice'] },
                                then: '$approvalDateForIsrael',
                                else: '$approvalDateForIttihad',
                            },
                        },
                        userId: 1,
                        pageId: 1,
                        like: 1,
                        likeCount: { $size: '$like' },
                        replyComments: {
                            $map: {
                                input: '$replyComments',
                                as: 'reply',
                                in: {
                                    commentReplay: '$$reply.commentReplay',
                                    likeCount: { $size: '$$reply.like' },
                                    like: '$$reply.like',
                                    _id: '$$reply._id',
                                },
                            },
                        },
                        totalReplay: { $size: '$replyComments' },
                        updatedByIsraelAdmin: {
                            $cond: {
                                if: { $eq: [{ $size: '$updatedByIsraelAdmin' }, 0] },
                                then: '$$REMOVE',
                                else: { $arrayElemAt: ['$updatedByIsraelAdmin', 0] },
                            },
                        },
                        updatedByIttihadAdmin: {
                            $cond: {
                                if: { $eq: [{ $size: '$updatedByIttihadAdmin' }, 0] },
                                then: '$$REMOVE',
                                else: { $arrayElemAt: ['$updatedByIttihadAdmin', 0] },
                            },
                        },
                        approvedByIsrael: {
                            $cond: {
                                if: { $eq: [{ $size: '$approvedByIsrael' }, 0] },
                                then: '$$REMOVE',
                                else: { $arrayElemAt: ['$approvedByIsrael', 0] },
                            },
                        },
                        approvedByIttihad: {
                            $cond: {
                                if: { $eq: [{ $size: '$approvedByIttihad' }, 0] },
                                then: '$$REMOVE',
                                else: { $arrayElemAt: ['$approvedByIttihad', 0] },
                            },
                        },
                    },
                },
                {
                    $match: queryObject,
                },
            ];

            let allCommentsData = await this.commentModel.aggregate(pipeline).sort(pagination.sort);

            const [israelUsers, ittihadUsers] = await Promise.all([
                this.israelUserModel.find().select('name').lean(),
                this.ittihadUserModel.find().select('name').lean(),
            ]);

            const users = [...israelUsers, ...ittihadUsers];

            allCommentsData.map((comment: any) => {
                if (comment.userId) {
                    let user = users.find((user) => user._id.toString() === comment.userId.toString());
                    if (user) {
                        comment.name = decrypt(user.name);
                    }
                } else {
                    comment.name = 'Anonymous';
                }
            });

            // let pageData = await this.commentModel.aggregate(pipeline);
            // let pageCount = Math.ceil(pageData.length / pagination.resultPerPage);

            allCommentsData = decryptArrayOfObject(allCommentsData, [
                'firstname',
                'lastname',
                'updatedByIsraelAdmin',
                'updatedByIttihadAdmin',
                'approvedByIsrael',
                'approvedByIttihad',
            ]);

            if (searchObject.sortField === 'name') {
                if (searchObject.sortOrder === 'asc') {
                    allCommentsData = allCommentsData.sort((a, b) => a.name.localeCompare(b.name));
                } else {
                    allCommentsData = allCommentsData.sort((a, b) => b.name.localeCompare(a.name));
                }
            }

            const page = pagination.page;
            const pageSize = pagination.resultPerPage;
            const startIndex = (page - 1) * pageSize;
            const endIndex = startIndex + pageSize;

            return {
                allCommentsData: allCommentsData.slice(startIndex, endIndex),
                pageCount: Math.ceil(allCommentsData.length / pagination.resultPerPage) || 0,
            };
        } catch (error) {
            console.log(error, 'error while loading comments');
            return error.message;
        }
    }

    //update comments data
    async updateCommentsData(commentId: string, commentsData: CreateCommentDto, request: any): Promise<any> {
        try {
            const permissions = request.user.permissions;
            if (!permissions.systemAdmins.write) {
                return returnMessage('permissionDenied');
            }

            let comment = await this.commentModel.findById(commentId).lean();
            if (!comment) comment = await this.commentReplayModel.findById(commentId).lean();
            if (!comment) return returnMessage('commentNotFound');

            const page = await this.pageModel.findById(comment.pageId).lean();
            if (!page) return returnMessage('pageNotFound');

            if (commentsData.originalComment) delete commentsData.originalComment;

            if (commentsData.status === 'approved') {
                if (request.user.site === 'israelBackOffice') {
                    if (page.israelStatus !== 'active') return returnMessage('pageNotApproved');
                    commentsData['updatedByIsrael'] = request.user._id;
                    commentsData['approvalDateForIsrael'] = new Date();
                    commentsData['israelStatus'] = commentsData.status;
                    commentsData['approvedByIsrael'] = request.user._id;
                } else if (request.user.site === 'ittihadBackOffice') {
                    if (page.ittihadStatus !== 'active') return returnMessage('pageNotApproved');
                    commentsData['updatedByIttihad'] = request.user._id;
                    commentsData['approvalDateForIttihad'] = new Date();
                    commentsData['ittihadStatus'] = commentsData.status;
                    commentsData['approvedByIttihad'] = request.user._id;
                }
            } else {
                if (request.user.site === 'israelBackOffice') {
                    if (page.israelStatus !== 'active') return returnMessage('pageNotApproved');
                    commentsData['israelStatus'] = commentsData.status;
                    if (request.body.status && request.body.status !== 'approved') {
                        commentsData['approvalDateForIsrael'] = null;
                        commentsData['approvedByIsrael'] = null;
                    }
                } else if (request.user.site === 'ittihadBackOffice') {
                    if (page.ittihadStatus !== 'active') return returnMessage('pageNotApproved');
                    commentsData['ittihadStatus'] = commentsData.status;
                    if (request.body.status && request.body.status !== 'approved') {
                        commentsData['approvalDateForIttihad'] = null;
                        commentsData['approvedByIttihad'] = null;
                    }
                }
            }

            if (commentsData.updatedComment && commentsData.updatedComment !== '') {
                const commentSanitize = await sanitizeComment(commentsData.updatedComment);
                if (commentSanitize) return returnMessage('commentSanitize');

                if (request.user.site === 'israelBackOffice') {
                    commentsData['updatedCommentByIsrael'] = commentsData.updatedComment;
                } else if (request.user.site === 'ittihadBackOffice') {
                    commentsData['updatedCommentByIttihad'] = commentsData.updatedComment;
                }
            }

            let updateData = await this.commentModel.findByIdAndUpdate(commentId, commentsData, { new: true });
            if (!updateData)
                updateData = await this.commentReplayModel.findByIdAndUpdate(commentId, commentsData, { new: true });

            if (!updateData) return returnMessage('commentNotFound');

            if (commentsData.status || commentsData.updatedComment) {
                let historyLogsData = {
                    logId: commentId,
                    method: 'Update',
                    data: commentsData,
                    site: updateData.site,
                    updatedBy: request.user._id,
                    module: 'comments',
                };

                await this.historyLogsService.addHistoryLog(historyLogsData);
            }

            const site = request.user.site === 'israelBackOffice' ? 'israelBackOffice' : 'ittihadBackOffice';
            const totalPendings = await this.userService.pendingCounts({ site });
            if (totalPendings) this.rabbitmqService.publish(`UpdatedCount:${site}`, JSON.stringify(totalPendings));
            const articlePageSite = request.user.site === 'israelBackOffice' ? 'israel' : 'ittihad';
            if (articlePageSite)
                this.rabbitmqService.publish(`${updateData.pageId}:${articlePageSite}`, 'Comment Updated');
            return updateData;
        } catch (error) {
            console.log(error, 'Getting error while updating comments');
            return error.message;
        }
    }

    //update like of comment
    async createLike(query: any, commentsData: CreateCommentDto, request: any): Promise<any> {
        try {
            if (request.headers.authorization && request.headers.authorization.startsWith('Bearer')) {
                const token = request.headers.authorization.split(' ')[1];
                const user = await this.userService.verifyArticlePageAuthToken(token);
                if (typeof user === 'string') return user;

                request['user'] = user;
            }

            if (request.user?._id) {
                commentsData.userId = request.user?._id;
            } else {
                commentsData.ip = commentsData.ip;
            }

            if (commentsData.like === true) {
                let commentExist = await this.commentModel.findById(query.commentId);
                let newModel: any = this.commentModel;

                if (!commentExist) {
                    commentExist = await this.commentReplayModel.findById(query.commentId);
                    newModel = this.commentReplayModel;
                }

                if (!commentExist) return returnMessage('commentNotExist');

                let array = commentExist.like;

                commentsData.commentId = query.commentId;

                let createLike = await this.CommentLikeModel.create(commentsData);

                if (createLike) {
                    array.push(createLike._id);
                    await newModel.findByIdAndUpdate(query.commentId, commentExist, { timestamps: false });
                }

                return { likeCount: commentExist.like.length || 0, like: true };
            } else {
                let pipeline = [
                    {
                        $lookup: {
                            from: 'commentlikes',
                            localField: 'like',
                            foreignField: '_id',
                            as: 'replyComments',
                        },
                    },
                    {
                        $project: {
                            _id: 1,
                            like: 1,
                            replyComments: {
                                $map: {
                                    input: '$replyComments',
                                    as: 'reply',
                                    in: {
                                        _id: '$$reply._id',
                                    },
                                },
                            },
                        },
                    },
                    {
                        $match: { _id: new mongoose.Types.ObjectId(query.commentId) },
                    },
                ];

                let findComment = await this.commentModel.aggregate(pipeline);

                let newModel: any = this.commentModel;

                if (findComment.length === 0) {
                    findComment = await this.commentReplayModel.aggregate(pipeline);
                    newModel = this.commentReplayModel;
                }

                if (findComment.length === 0) return returnMessage('commentNotExist');

                let obj = {
                    commentId: query.commentId,
                };

                if (request.user?._id) {
                    obj['userId'] = request.user._id;
                } else {
                    obj['ip'] = commentsData.ip;
                }

                let likeId = await this.CommentLikeModel.findOne(obj);

                let removeLike = findComment[0].like.filter(
                    (item: any) => item?._id?.toString() !== likeId?._id?.toString(),
                );

                await newModel.findByIdAndUpdate(query.commentId, { like: removeLike }, { timestamps: false });

                await this.CommentLikeModel.deleteOne(likeId._id);

                return { likeCount: removeLike.length || 0, like: false };
            }
        } catch (error) {
            console.log('error while updating Like', error);
            return error.message;
        }
    }

    //Get comment By ID
    async getCommentByID(commentId: string, request: any): Promise<any> {
        try {
            const permissions = request.user.permissions;
            if (!permissions.systemAdmins.read) {
                return returnMessage('permissionDenied');
            }

            const pipeline = [
                {
                    $lookup: {
                        from: 'admins',
                        localField: 'updatedByIsrael',
                        foreignField: '_id',
                        as: 'updatedByIsraelAdmin',
                        pipeline: [{ $project: { firstname: 1, lastname: 1, fullname: 1 } }],
                    },
                },
                {
                    $lookup: {
                        from: 'admins',
                        localField: 'updatedByIttihad',
                        foreignField: '_id',
                        as: 'updatedByIttihadAdmin',
                        pipeline: [{ $project: { firstname: 1, lastname: 1, fullname: 1 } }],
                    },
                },
                {
                    $lookup: {
                        from: 'admins',
                        localField: 'approvedByIttihad',
                        foreignField: '_id',
                        as: 'approvedByIttihad',
                        pipeline: [{ $project: { firstname: 1, lastname: 1, fullname: 1 } }],
                    },
                },
                {
                    $lookup: {
                        from: 'admins',
                        localField: 'approvedByIsrael',
                        foreignField: '_id',
                        as: 'approvedByIsrael',
                        pipeline: [{ $project: { firstname: 1, lastname: 1, fullname: 1 } }],
                    },
                },
                {
                    $lookup: {
                        from: 'pages',
                        localField: 'pageId',
                        foreignField: '_id',
                        as: 'pageData',
                        pipeline: [{ $project: { israelPage: 1, ittihadPage: 1, ittihadUrl: 1, isrealUrl: 1 } }],
                    },
                },
                { $unwind: '$pageData' },
                {
                    $match: { _id: new mongoose.Types.ObjectId(commentId) },
                },
                {
                    $project: {
                        _id: 1,
                        row_id: 1,
                        ip: 1,
                        originalComment: 1,
                        updatedCommentByIsrael: 1,
                        updatedCommentByIttihad: 1,
                        israelTranslation: 1,
                        ittihadTranslation: 1,
                        site: 1,
                        status: {
                            $cond: {
                                if: { $eq: [request?.user?.site, 'israelBackOffice'] },
                                then: '$israelStatus',
                                else: '$ittihadStatus',
                            },
                        },
                        pageData: 1,
                        userId: 1,
                        createdAt: 1,
                        updatedAt: 1,
                        approvalDate: {
                            $cond: {
                                if: { $eq: [request?.user?.site, 'israelBackOffice'] },
                                then: '$approvalDateForIsrael',
                                else: '$approvalDateForIttihad',
                            },
                        },
                        updatedByIsraelAdmin: {
                            $cond: {
                                if: { $eq: [{ $size: '$updatedByIsraelAdmin' }, 0] },
                                then: '$$REMOVE',
                                else: { $arrayElemAt: ['$updatedByIsraelAdmin', 0] },
                            },
                        },
                        updatedByIttihadAdmin: {
                            $cond: {
                                if: { $eq: [{ $size: '$updatedByIttihadAdmin' }, 0] },
                                then: '$$REMOVE',
                                else: { $arrayElemAt: ['$updatedByIttihadAdmin', 0] },
                            },
                        },
                        approvedByIsrael: {
                            $cond: {
                                if: { $eq: [{ $size: '$approvedByIsrael' }, 0] },
                                then: '$$REMOVE',
                                else: { $arrayElemAt: ['$approvedByIsrael', 0] },
                            },
                        },
                        approvedByIttihad: {
                            $cond: {
                                if: { $eq: [{ $size: '$approvedByIttihad' }, 0] },
                                then: '$$REMOVE',
                                else: { $arrayElemAt: ['$approvedByIttihad', 0] },
                            },
                        },
                    },
                },
            ];

            let findComment = await this.commentModel.aggregate(pipeline);

            if (findComment.length === 0) findComment = await this.commentReplayModel.aggregate(pipeline);
            if (findComment.length === 0) return returnMessage('commentNotFound');

            let findUser: any;
            if (findComment[0].userId) {
                findUser = await this.ittihadUserModel.findById(findComment[0].userId).lean();

                if (!findUser) {
                    findUser = await this.israelUserModel.findById(findComment[0].userId).lean();
                }
            }

            if (findUser) {
                findComment.map((item: any) => {
                    item['name'] = decrypt(findUser.name);
                    item['ip'] = findUser.ip;
                    item['email'] = decrypt(findUser.email);
                });
            }

            return decryptArrayOfObject(findComment, [
                'firstname',
                'lastname',
                'updatedByIsraelAdmin',
                'updatedByIttihadAdmin',
                'approvedByIttihad',
                'approvedByIsrael',
            ]);
        } catch (error) {
            console.log(error, 'error while loading comments');
            return error.message;
        }
    }

    async downloadExcel(request: any) {
        try {
            let queryObject: any = {
                site: request.user.site === 'israelBackOffice' ? 'israel-today' : 'ittihad-today',
            };
            const branches = [
                {
                    case: {
                        $and: [
                            { $eq: [request?.user?.site, 'israelBackOffice'] },
                            { $eq: ['$site', 'israel-today'] },
                            {
                                $ne: [{ $ifNull: ['$updatedCommentByIsrael', 'undefined'] }, 'undefined'],
                            },
                        ],
                    },
                    then: {
                        dd: 259,
                        comment: '$updatedCommentByIsrael',
                    },
                },
                {
                    case: {
                        $and: [
                            { $eq: [request?.user?.site, 'israelBackOffice'] },
                            { $eq: ['$site', 'israel-today'] },
                            {
                                $eq: [{ $ifNull: ['$updatedCommentByIsrael', 'undefined'] }, 'undefined'],
                            },
                        ],
                    },
                    then: {
                        dd: 274,
                        comment: '$originalComment',
                    },
                },
                {
                    case: {
                        $and: [
                            { $eq: [request?.user?.site, 'ittihadBackOffice'] },
                            { $eq: ['$site', 'ittihad-today'] },
                            {
                                $ne: [{ $ifNull: ['$updatedCommentByIttihad', 'undefined'] }, 'undefined'],
                            },
                        ],
                    },
                    then: {
                        dd: 289,
                        comment: '$updatedCommentByIttihad',
                    },
                },
                {
                    case: {
                        $and: [
                            { $eq: [request?.user?.site, 'ittihadBackOffice'] },
                            { $eq: ['$site', 'ittihad-today'] },
                            {
                                $eq: [{ $ifNull: ['$updatedCommentByIttihad', 'undefined'] }, 'undefined'],
                            },
                        ],
                    },
                    then: {
                        dd: 304,
                        comment: '$originalComment',
                    },
                },
                {
                    case: {
                        $and: [
                            { $eq: [request?.user?.site, 'israelBackOffice'] },
                            { $eq: ['$site', 'ittihad-today'] },
                            {
                                $ne: [{ $ifNull: ['$updatedCommentByIsrael', 'undefined'] }, 'undefined'],
                            },
                            {
                                $ne: [{ $ifNull: ['$updatedCommentByIttihad', 'undefined'] }, 'undefined'],
                            },
                        ],
                    },
                    then: {
                        dd: 322,
                        comment: '$updatedCommentByIsrael',
                    },
                },
                {
                    case: {
                        $and: [
                            { $eq: [request?.user?.site, 'israelBackOffice'] },
                            { $eq: ['$site', 'ittihad-today'] },
                            {
                                $ne: [{ $ifNull: ['$updatedCommentByIsrael', 'undefined'] }, 'undefined'],
                            },
                            {
                                $eq: [{ $ifNull: ['$updatedCommentByIttihad', 'undefined'] }, 'undefined'],
                            },
                        ],
                    },
                    then: {
                        dd: 341,
                        comment: '$updatedCommentByIsrael',
                    },
                },
                {
                    case: {
                        $and: [
                            { $eq: [request?.user?.site, 'israelBackOffice'] },
                            { $eq: ['$site', 'ittihad-today'] },
                            {
                                $eq: [{ $ifNull: ['$updatedCommentByIsrael', 'undefined'] }, 'undefined'],
                            },
                            {
                                $ne: [{ $ifNull: ['$updatedCommentByIttihad', 'undefined'] }, 'undefined'],
                            },
                        ],
                    },
                    then: {
                        dd: 360,
                        comment: '$ittihadTranslation',
                    },
                },
                {
                    case: {
                        $and: [
                            { $eq: [request?.user?.site, 'israelBackOffice'] },
                            { $eq: ['$site', 'ittihad-today'] },
                            {
                                $eq: [{ $ifNull: ['$updatedCommentByIsrael', 'undefined'] }, 'undefined'],
                            },
                            {
                                $eq: [{ $ifNull: ['$updatedCommentByIttihad', 'undefined'] }, 'undefined'],
                            },
                        ],
                    },
                    then: {
                        dd: 379,
                        comment: '$ittihadTranslation',
                    },
                },
                {
                    case: {
                        $and: [
                            { $eq: [request?.user?.site, 'ittihadBackOffice'] },
                            { $eq: ['$site', 'israel-today'] },
                            {
                                $ne: [{ $ifNull: ['$updatedCommentByIsrael', 'undefined'] }, 'undefined'],
                            },
                            {
                                $ne: [{ $ifNull: ['$updatedCommentByIttihad', 'undefined'] }, 'undefined'],
                            },
                        ],
                    },
                    then: {
                        dd: 398,
                        comment: '$updatedCommentByIttihad',
                    },
                },
                {
                    case: {
                        $and: [
                            { $eq: [request?.user?.site, 'ittihadBackOffice'] },
                            { $eq: ['$site', 'israel-today'] },
                            {
                                $ne: [{ $ifNull: ['$updatedCommentByIttihad', 'undefined'] }, 'undefined'],
                            },
                            {
                                $eq: [{ $ifNull: ['$updatedCommentByIsrael', 'undefined'] }, 'undefined'],
                            },
                        ],
                    },
                    then: {
                        dd: 417,
                        comment: '$updatedCommentByIttihad',
                    },
                },
                {
                    case: {
                        $and: [
                            { $eq: [request?.user?.site, 'ittihadBackOffice'] },
                            { $eq: ['$site', 'israel-today'] },
                            {
                                $eq: [{ $ifNull: ['$updatedCommentByIttihad', 'undefined'] }, 'undefined'],
                            },
                            {
                                $ne: [{ $ifNull: ['$updatedCommentByIsrael', 'undefined'] }, 'undefined'],
                            },
                        ],
                    },
                    then: {
                        dd: 436,
                        comment: '$israelTranslation',
                    },
                },
                {
                    case: {
                        $and: [
                            { $eq: [request?.user?.site, 'ittihadBackOffice'] },
                            { $eq: ['$site', 'israel-today'] },
                            {
                                $eq: [{ $ifNull: ['$updatedCommentByIsrael', 'undefined'] }, 'undefined'],
                            },
                            {
                                $eq: [{ $ifNull: ['$updatedCommentByIttihad', 'undefined'] }, 'undefined'],
                            },
                        ],
                    },
                    then: {
                        dd: 455,
                        comment: '$israelTranslation',
                    },
                },
            ];

            const pipeline = [
                {
                    $unionWith: {
                        coll: 'commentreplays',
                    },
                },
                {
                    $lookup: {
                        from: 'admins',
                        localField: 'approvedByIsrael',
                        foreignField: '_id',
                        as: 'approvedByIsraelAdmin',
                        pipeline: [{ $project: { firstname: 1, lastname: 1, fullname: 1 } }],
                    },
                },
                {
                    $lookup: {
                        from: 'admins',
                        localField: 'approvedByIttihad',
                        foreignField: '_id',
                        as: 'approvedByIttihadAdmin',
                        pipeline: [{ $project: { firstname: 1, lastname: 1, fullname: 1 } }],
                    },
                },

                {
                    $lookup: {
                        from: 'pages',
                        localField: 'pageId',
                        foreignField: '_id',
                        as: 'pageData',
                        pipeline: [{ $project: { israelPage: 1, ittihadPage: 1 } }],
                    },
                },
                { $unwind: '$pageData' },
                {
                    $project: {
                        _id: 0,
                        row_id: 1,
                        originalComment: 1,
                        site: 1,
                        comment: {
                            $switch: {
                                branches: branches,
                                default: null,
                            },
                        },
                        status: {
                            $cond: {
                                if: { $eq: ['$site', 'israel-today'] },
                                then: '$israelStatus',
                                else: '$ittihadStatus',
                            },
                        },
                        userId: 1,
                        ip: 1,
                        pageData: {
                            $cond: {
                                if: { $eq: ['$site', 'israel-today'] },
                                then: '$pageData.israelPage',
                                else: '$pageData.ittihadPage',
                            },
                        },
                        createdAt: 1,
                        approvalDate: {
                            $cond: {
                                if: { $eq: ['$site', 'israel-today'] },
                                then: '$approvalDateForIsrael',
                                else: '$approvalDateForIttihad',
                            },
                        },
                        approvedByIsraelAdmin: {
                            $cond: {
                                if: { $eq: [{ $size: '$approvedByIsraelAdmin' }, 0] },
                                then: '$$REMOVE',
                                else: { $arrayElemAt: ['$approvedByIsraelAdmin', 0] },
                            },
                        },
                        approvedByIttihadAdmin: {
                            $cond: {
                                if: { $eq: [{ $size: '$approvedByIttihadAdmin' }, 0] },
                                then: '$$REMOVE',
                                else: { $arrayElemAt: ['$approvedByIttihadAdmin', 0] },
                            },
                        },
                    },
                },
                {
                    $match: queryObject,
                },
            ];
            let allCommentsData: any = await this.commentModel.aggregate(pipeline);

            const [israelUsers, ittihadUsers] = await Promise.all([
                this.israelUserModel.find().select('name').lean(),
                this.ittihadUserModel.find().select('name').lean(),
            ]);

            const users = [...israelUsers, ...ittihadUsers];

            allCommentsData.map((comment: any) => {
                if (comment.userId) {
                    let user = users.find((user) => user._id.toString() === comment.userId.toString());
                    if (user) comment.userName = decrypt(user.name);
                } else {
                    comment.userName = 'Anonymous';
                }
            });

            return this.optimiseExcel(allCommentsData);
        } catch (error) {
            console.log(error, 'error while loading comments');
            return error.message;
        }
    }

    optimiseExcel(data: any[]): any {
        try {
            const batchSize = Math.ceil(Math.sqrt(data.length));
            let chunkArray: any[] = [];
            for (let index = 0; index < batchSize; index++) {
                chunkArray.push(data.splice(0, batchSize));
            }

            let updatedData = chunkArray.map((element: any) => {
                return this.optimisedCommnetXls(element);
            });
            return [].concat.apply([], updatedData);
        } catch (error) {
            console.log('error while exporting Comments data', error);
            return error.message;
        }
    }

    optimisedCommnetXls(data: any[]): any {
        try {
            const newArrayData: any[] = [];
            data.forEach((element: any) => {
                let obj = {};
                obj['Id'] = element.row_id || '-';
                if (element.status) {
                    if (element.status === 'approved') obj['Status'] = 'Approved';
                    else if (element.status === 'pending') obj['Status'] = 'In-Active';
                    else if (element.status === 'notApproved') obj['Status'] = 'Deleted';
                } else obj['Status'] = '-';

                obj['Site'] = element.site === 'israel-today' ? 'Israel-Today' : 'Ittihad-Today';
                obj['User'] = capitalizeFirstLetter(element.userName) || '-';
                obj['Page name'] = element.pageData;
                obj['Comment'] = element?.comment?.comment || '-';
                obj['Submit date'] = element.createdAt
                    ? moment(element.createdAt).tz('Israel').format('DD.MM.YYYY HH:mm')
                    : '-';
                obj['Approval date'] = element.approvalDate
                    ? moment(element.approvalDate).tz('Israel').format('DD.MM.YYYY HH:mm')
                    : '-';

                if (element.approvedByIsraelAdmin) {
                    obj['Approval admin'] =
                        capitalizeFirstLetter(decrypt(element?.approvedByIsraelAdmin?.firstname)) +
                            ' ' +
                            capitalizeFirstLetter(decrypt(element?.approvedByIsraelAdmin?.lastname)) || '-';
                } else if (element.approvedByIttihadAdmin) {
                    obj['Approval admin'];
                    capitalizeFirstLetter(decrypt(element?.approvedByIttihadAdmin?.firstname)) +
                        ' ' +
                        capitalizeFirstLetter(decrypt(element?.approvedByIttihadAdmin?.lastname)) || '-';
                }
                newArrayData.push(obj);
            });

            return newArrayData;
        } catch (error) {
            console.log('error while exporting Comments data', error);
            return error.message;
        }
    }

    async checkForMaxComment(user: any): Promise<any> {
        try {
            const searchObj = {
                createdAt: {
                    $gte: moment().tz('Asia/Kolkata').subtract(1, 'minute'),
                    $lt: moment().tz('Asia/Kolkata'),
                },
            };
            if (user?.id) {
                searchObj['userId'] = user?.id;
            } else if (user?.ip) {
                searchObj['ip'] = user?.ip;
            }
            const [totalComments, totalReplyComments] = await Promise.all([
                this.commentModel.countDocuments(searchObj),
                this.commentReplayModel.countDocuments(searchObj),
            ]);
            if (totalComments + totalReplyComments >= 10) return true;
            return false;
        } catch (error) {
            console.log('error in max comment count', error);
            return error.message;
        }
    }
}
