import { HttpException } from '@nestjs/common';
import appMessage from './appMessages.json';
import arabicMessage from './arabicMessages.json';
import hebrewMessage from './hebrewMessages.json';

export class CustomError extends HttpException {
    constructor(data: any, statusCode: any) {
        if (
            data.message == appMessage['permissionDenied'] ||
            data.message == appMessage['tokenNotExist'] ||
            data.message == appMessage['mustLogin'] ||
            data.message == arabicMessage['permissionDenied'] ||
            data.message == arabicMessage['tokenNotExist'] ||
            data.message == arabicMessage['mustLogin'] ||
            data.message == hebrewMessage['permissionDenied'] ||
            data.message == hebrewMessage['tokenNotExist'] ||
            data.message == hebrewMessage['mustLogin']
        )
            statusCode = 401;

        if (data.message == appMessage['tokenNotExistOnArticle']) {
            statusCode = 401;
            data['message'] = undefined;
        }

        super(data, statusCode);
    }
}
