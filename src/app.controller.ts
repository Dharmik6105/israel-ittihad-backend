import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { ApiExcludeEndpoint } from '@nestjs/swagger';
@Controller(`/api/users`)
export class AppController {
    constructor(private readonly appService: AppService) {}
    @ApiExcludeEndpoint()
    @Get('/user_details')
    getHello(): string {
        return this.appService.getHello();
    }
}
