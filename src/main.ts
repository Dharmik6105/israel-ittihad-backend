import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as dotenv from 'dotenv';
import { HttpExceptionFilter } from './global.error.middleware';
import { ValidationPipe } from '@nestjs/common';
import { setupSwagger } from 'swagger.config';
import { NestExpressApplication } from '@nestjs/platform-express';
import { join } from 'path';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { MqttOptions } from 'src/rabbitmq/rabbitmq.config';
import mongoSanitize from 'express-mongo-sanitize';

dotenv.config();

async function bootstrap() {
    let app = await NestFactory.create<NestExpressApplication>(AppModule);

    setupSwagger(app);
    app.enableCors();
    app.use(mongoSanitize());
    // app.use(helemt());
    app.useStaticAssets(join(__dirname, '..', 'public'));
    app.connectMicroservice<MicroserviceOptions>({
        transport: Transport.MQTT,
        options: MqttOptions.options,
    });

    await app.startAllMicroservices();

    await app.listen(process.env.PORT || 3000, () => {
        console.log(`Server started at port ${process.env.PORT}`);
    });

    app.useGlobalPipes(new ValidationPipe({ whitelist: true }));
    app.useGlobalFilters(new HttpExceptionFilter());
}

bootstrap();
