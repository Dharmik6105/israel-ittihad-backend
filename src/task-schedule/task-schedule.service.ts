import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Cron } from '@nestjs/schedule';
import moment from 'moment-timezone';
import { Model } from 'mongoose';
import { CommentDocument, Comments } from 'src/comments/schema/comment.schema';
import { CommentReplay, CommentReplayDocument } from 'src/comments/schema/commentReplay.schema';
import { PageSettingDocument, page_setting } from 'src/page-setting/page-setting.schema';
import { Page, PageDocument } from 'src/pages/pages.schema';
import { UserService } from 'src/user/user.service';
import * as dotenv from 'dotenv';
dotenv.config();

@Injectable()
export class TaskScheduleService {
    constructor(
        @InjectModel(Comments.name, 'SYSTEM_DB')
        private readonly Comment_model: Model<CommentDocument>,
        @InjectModel(CommentReplay.name, 'SYSTEM_DB')
        private readonly Comment_reply_model: Model<CommentReplayDocument>,
        @InjectModel(page_setting.name, 'SYSTEM_DB')
        private readonly Page_Setting_model: Model<PageSettingDocument>,
        @InjectModel(Page.name, 'SYSTEM_DB')
        private readonly Page_model: Model<PageDocument>,
        private readonly userService: UserService,
    ) {}

    @Cron(process.env.CRONTIME, {
        name: 'Comment Notification',
        timeZone: 'Asia/Kolkata',
    })
    async commentNotificationTaskSchedule() {
        try {
            console.log('Cron Job Started');
            const currentTime = moment().utc().tz('Asia/Kolkata');
            const pastTime = moment().utc().tz('Asia/Kolkata').subtract(process.env.CRON_TIME_STAMP, 'minutes');
            const pastCronTime = moment().utc().tz('Asia/Kolkata').subtract(process.env.CRON_TIME_STAMP, 'minutes');
            console.log(currentTime, pastTime, pastCronTime);
            const [comments, replyComments, israelPageSetting, ittihadPageSetting, pages] = await Promise.all([
                this.Comment_model.find().lean(),
                this.Comment_reply_model.find().lean(),
                this.Page_Setting_model.findOne({ site: 'israelBackOffice' }).lean(),
                this.Page_Setting_model.findOne({ site: 'ittihadBackOffice' }).lean(),
                this.Page_model.find({ $or: [{ israelStatus: 'active' }, { ittihadStatus: 'active' }] }).lean(),
            ]);
            console.log(pages.length, 46);
            const totalComments = [...comments, ...replyComments];
            let israelTableRow = `
            <thead>
		        <tr>
                    <td style="font-weight: bolder; padding: 15px; border: 1px solid black;">#</td>
                    <td style="font-weight: bolder; padding: 15px; border: 1px solid black;">Page name</td>
                    <td style="font-weight: bolder; padding: 15px; border: 1px solid black;">Page URL</td>
                    <td style="font-weight: bolder; padding: 15px; border: 1px solid black;">No of new comment</td>
		        </tr>
	        </thead>`;

            let ittihadTableRow = `
            <thead>
		        <tr>
                    <td style="font-weight: bolder; padding: 15px; border: 1px solid black;">#</td>
                    <td style="font-weight: bolder; padding: 15px; border: 1px solid black;">Page name</td>
                    <td style="font-weight: bolder; padding: 15px; border: 1px solid black;">Page URL</td>
                    <td style="font-weight: bolder; padding: 15px; border: 1px solid black;">No of new comment</td>
		        </tr>
	        </thead>`;

            let israelCounter = 0,
                ittihadCounter = 0;
            pages.forEach((page: any) => {
                console.log('inside the israel comments loop');
                if (page.israelStatus === 'active' && page.isrealUrl && page.israelPage) {
                    const comments = totalComments.filter(
                        (comment: any) =>
                            moment(comment.createdAt).tz('Asia/Kolkata').isSameOrAfter(pastTime) &&
                            moment(comment.createdAt).tz('Asia/Kolkata').isBefore(currentTime) &&
                            comment['site'] === 'israel-today' &&
                            comment['pageId'].toString() == page?._id?.toString(),
                    );

                    console.log(comments.length, 75);
                    if (comments.length !== 0) {
                        israelTableRow += `
                        <tr>
                        <td style="padding: 15px; border: 1px solid black;">${++israelCounter}</td>
                        <td style="padding: 15px; border: 1px solid black;" >
                        <a style="color: blue; text-decoration: underline;" href="${process.env.WEBHOST}/pages/edit/${
                            page._id
                        }" target="_blank">${page['israelPage']}</a></td>
                        <td style="padding: 15px; border: 1px solid black;"> 
                        <a style="color: blue; text-decoration: underline;" href="${
                            page['isrealUrl']
                        }" target="_blank">${page['isrealUrl']}</a></td>
                        <td style="padding: 15px; border: 1px solid black;">${comments.length}</td>
                        </tr>`;
                    }
                }
                if (page.ittihadStatus === 'active' && page.ittihadUrl && page.ittihadPage) {
                    const comments = totalComments.filter(
                        (comment: any) =>
                            moment(comment.createdAt).tz('Asia/Kolkata').isSameOrAfter(pastCronTime) &&
                            comment['site'] === 'ittihad-today' &&
                            comment['pageId'].toString() == page._id.toString() &&
                            comment['ittihadStatus'] == 'approved',
                    );
                    if (comments.length !== 0) {
                        israelTableRow += `<tr>
                        <td style="padding: 15px; border: 1px solid black;">${++israelCounter}</td>
                        <td style="padding: 15px; border: 1px solid black;">
                        <a style="color: blue; text-decoration: underline;" href="${process.env.WEBHOST}/pages/edit/${
                            page._id
                        }" target="_blank">${page['ittihadPage']}</a></td>
                        <td style="padding: 15px; border: 1px solid black;">
                        <a style="color: blue; text-decoration: underline;" href="${
                            page['ittihadUrl']
                        }" target="_blank">${page['ittihadUrl']}</a></td>
                        <td style="padding: 15px; border: 1px solid black;">${comments.length}</td>
                    </tr>`;
                    }
                }
            });

            pages.forEach((page: any) => {
                if (page.ittihadStatus === 'active' && page.ittihadUrl && page.ittihadPage) {
                    const comments = totalComments.filter(
                        (comment: any) =>
                            moment(comment.createdAt).tz('Asia/Kolkata').isSameOrAfter(pastTime) &&
                            moment(comment.createdAt).tz('Asia/Kolkata').isBefore(currentTime) &&
                            comment['site'] === 'ittihad-today' &&
                            comment['pageId'].toString() == page._id.toString(),
                    );
                    if (comments.length !== 0) {
                        ittihadTableRow += `<tr>
                        <td style="padding: 15px; border: 1px solid black;">${++ittihadCounter}</td>
                        <td style="padding: 15px; border: 1px solid black;">
                        <a style="color: blue; text-decoration: underline;" href="${process.env.WEBHOST}/pages/edit/${
                            page._id
                        }" target="_blank">${page['ittihadPage']}</a></td>
                        <td style="padding: 15px; border: 1px solid black;">
                        <a style="color: blue; text-decoration: underline;" href="${
                            page['ittihadUrl']
                        }" target="_blank">${page['ittihadUrl']}</a></td>
                        <td style="padding: 15px; border: 1px solid black;">${comments.length}</td>
                         </tr>`;
                    }
                }
                if (page.israelStatus === 'active' && page.isrealUrl && page.israelPage) {
                    const comments = totalComments.filter(
                        (comment: any) =>
                            moment(comment.createdAt).tz('Asia/Kolkata').isSameOrAfter(pastCronTime) &&
                            comment['site'] === 'israel-today' &&
                            comment['pageId'].toString() == page._id.toString() &&
                            comment['israelStatus'] === 'approved',
                    );
                    console.log(comments.length, 140);
                    if (comments.length !== 0) {
                        ittihadTableRow += `<tr>
                        <td style="padding: 15px; border: 1px solid black;">${++ittihadCounter}</td>
                        <td style="padding: 15px; border: 1px solid black;">
                        <a style="color: blue; text-decoration: underline;" href="${process.env.WEBHOST}/pages/edit/${
                            page._id
                        }" target="_blank">${page['israelPage']}</a></td>
                        <td style="padding: 15px; border: 1px solid black;">
                        <a style="color: blue; text-decoration: underline;" href="${
                            page['isrealUrl']
                        }</" target="_blank">${page['isrealUrl']}</a></td>
                        <td style="padding: 15px; border: 1px solid black;">${comments.length}</td>
                       </tr>`;
                    }
                }
            });
            if (israelCounter > 0) {
                let israelEmails = israelPageSetting.comment_notifications[0];
                israelTableRow = `<table>${israelTableRow}</table>`;
                israelPageSetting.newcomment_email_message = israelPageSetting.newcomment_email_message.replaceAll(
                    '{{comment_text}}',
                    israelTableRow,
                );
                israelPageSetting.newcomment_email_message = israelPageSetting.newcomment_email_message.replaceAll(
                    '{{site_name}}',
                    'Israel-Today',
                );

                JSON.parse(israelEmails).map((email: string) => {
                    this.userService.sendMail(
                        israelPageSetting.newcomment_email_from,
                        email,
                        israelPageSetting.newcomment_email_reply,
                        israelPageSetting.newcomment_email_sub,
                        israelPageSetting.newcomment_email_message,
                    );
                });
            }
            if (ittihadCounter > 0) {
                let ittihadEmails = ittihadPageSetting.comment_notifications[0];
                ittihadTableRow = `<table>${ittihadTableRow}</table>`;
                ittihadPageSetting.newcomment_email_message = ittihadPageSetting.newcomment_email_message.replaceAll(
                    '{{comment_text}}',
                    ittihadTableRow,
                );
                ittihadPageSetting.newcomment_email_message = ittihadPageSetting.newcomment_email_message.replaceAll(
                    '{{site_name}}',
                    'Ittihad-Today',
                );
                JSON.parse(ittihadEmails).map((email: string) => {
                    this.userService.sendMail(
                        ittihadPageSetting.newcomment_email_from,
                        email,
                        ittihadPageSetting.newcomment_email_reply,
                        ittihadPageSetting.newcomment_email_sub,
                        ittihadPageSetting.newcomment_email_message,
                    );
                });
            }
        } catch (error) {
            console.log('error in the Comment notification task schedular', error);
        }
    }
}
