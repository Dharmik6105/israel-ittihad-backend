import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsString } from 'class-validator';

export class ResendOtpDto {
    @IsString()
    @IsNotEmpty()
    @IsEmail()
    @ApiProperty({ default: 'admintest@yopmail.com' })
    email: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty({
        default: 'israel-today',
        description: 'add the site of the User israel-today or ittihad-today',
        required: true,
    })
    site: string;
}
