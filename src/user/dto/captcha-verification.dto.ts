import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class CaptchaVerificationDto {
    @IsString()
    @IsNotEmpty()
    @ApiProperty({ default: 'token' })
    token: string;
}
