import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsString } from 'class-validator';

export class ForgotPasswordDto {
    @IsString()
    @IsNotEmpty()
    @IsEmail()
    @ApiProperty({ default: 'admintest@yopmail.com' })
    email: string;

    @IsString()
    @ApiProperty({
        default: 'israel-today',
        description: 'add the site of the User israel-today or ittihad-today',
    })
    site: string;
}
