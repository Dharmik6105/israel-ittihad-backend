import { Admin, AdminDocument } from 'src/admin/admin.schema';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateUserDto } from './dto/create-user.dto';
import {
    capitalizeFirstLetter,
    emailTemplate,
    getKeywordType,
    paginationObject,
    returnMessage,
} from 'src/helpers/utils';
import * as jwt from 'jsonwebtoken';
import * as bcrypt from 'bcryptjs';
import * as crypto from 'crypto';
import { MailerService } from '@nestjs-modules/mailer';
import { LoginUserDto } from './dto/login-user.dto';
import { VerifyOtpDto } from './dto/verify-otp.dto';
import { ResetPasswordDto } from './dto/reset-password.dto';
import { CreateSearchObjectDto } from 'src/common/search_object.dto';
import { Role, RoleDocument } from 'src/role_permission/role.schema';
import { ForgotPasswordDto } from './dto/forgot-password.dto';
import { decrypt, decryptArrayOfObject, decryptObjectFields, encrypt } from 'src/helpers/encrypt-decrypt';
import { ArticlePageUser, ArticlePageUserDocument } from './schema/article-page-user.schema';
import { ArticlePageRegisterUserDto } from './dto/article-page-create-user.dto';
import { PageSettingDocument, page_setting } from 'src/page-setting/page-setting.schema';
import { EmailVerifyUserDto } from './dto/email-verify-user.dto';
import { LoginArticlePageDto } from './dto/login-article-page.dto';
import { CommentDocument, Comments } from 'src/comments/schema/comment.schema';
import { Page, PageDocument } from 'src/pages/pages.schema';
import { ResetPasswordArticlePageDto } from './dto/reset-password-article-page.dto';
import { ResendOtpDto } from './dto/resend-otp.dto';
import moment from 'moment-timezone';
import { GoogleSignInDto } from './dto/google-signin.dto';
import { CommentReplay, CommentReplayDocument } from 'src/comments/schema/commentReplay.schema';
import axios from 'axios';
import { CaptchaVerificationDto } from './dto/captcha-verification.dto';

@Injectable()
export class UserService {
    constructor(
        @InjectModel(Admin.name, 'SYSTEM_DB')
        private readonly Admin: Model<AdminDocument>,
        @InjectModel(Role.name, 'SYSTEM_DB')
        private readonly Role: Model<RoleDocument>,
        @InjectModel(ArticlePageUser.name, 'ISRAEL_DB')
        private readonly ISRAEL_User: Model<ArticlePageUserDocument>,
        @InjectModel(ArticlePageUser.name, 'ITTIHAD_DB')
        private readonly ITTIHAD_User: Model<ArticlePageUserDocument>,
        @InjectModel(page_setting.name, 'SYSTEM_DB')
        private readonly PageSettingModel: Model<PageSettingDocument>,
        @InjectModel(Page.name, 'SYSTEM_DB')
        private readonly Page: Model<PageDocument>,
        @InjectModel(Comments.name, 'SYSTEM_DB') private readonly CommnetsModel: Model<CommentDocument>,
        @InjectModel(CommentReplay.name, 'SYSTEM_DB') private readonly CommentReplyModel: Model<CommentReplayDocument>,
        private readonly mailerService: MailerService,
    ) {}

    googleLogin(request: any) {
        if (!request.user) return 'default';
        return request.user;
    }

    async register(reqBody: CreateUserDto): Promise<any> {
        try {
            const { email, password, firstname, lastname, phone, site, status, user_type } = reqBody;
            if (!email || !password || !firstname || !lastname || !phone || !site || !status || !user_type)
                return returnMessage('fieldsMissing');

            const userExist = await this.Admin.findOne({
                email: encrypt(reqBody.email),
            }).lean();
            if (userExist && userExist.is_deleted === false) return returnMessage('userExist');
            else if (userExist && userExist.is_deleted === true) {
            }

            reqBody.password = await bcrypt.hash(reqBody.password, 12);

            // for encryption of the users data
            reqBody.email = encrypt(reqBody.email);
            reqBody.firstname = encrypt(reqBody.firstname);
            reqBody.lastname = encrypt(reqBody.lastname);
            reqBody.phone = encrypt(reqBody.phone);

            const user = await this.Admin.create(reqBody);

            if (!user) return returnMessage('default');

            const token = jwt.sign({ id: user._id, site }, process.env.JWT_SECRET, {
                expiresIn: process.env.JWT_EXPIRE,
            });

            return { token, user };
        } catch (error) {
            console.log('error in register user', error);
            return error.message;
        }
    }

    // this function is used for the login of system-back-office, israel-back-office and ittihad
    async login(reqBody: LoginUserDto): Promise<any> {
        try {
            if (!reqBody.email || !reqBody.password) return returnMessage('emailPassNotFound');

            const user = await this.Admin.findOne({
                email: encrypt(reqBody.email),
                is_deleted: false,
            });

            if (!user) return returnMessage('userNotFound');

            if (user.status === 'inActive') return returnMessage('accountSuspended');

            const comparePassword = await bcrypt.compare(reqBody.password, user.password);

            if (!comparePassword) return returnMessage('incorrectEmailPassword');

            user.otp = Math.floor(100000 + Math.random() * 900000);
            user.otpExpire = Date.now() + 900000;
            await user.save();
            const subject = 'OTP for login';
            const message = `Your OTP for Login is ${user.otp}<br>
      <br><b>NOTE: OTP is valid for the 15 minutes Only.</b>
      `;

            this.sendMail('', decrypt(user.email), '', subject, message);
            // if (typeof sentMail === 'string') return returnMessage('ErrorEmail');
            return true;
        } catch (error) {
            console.log('error in login api', error);
            return error.message;
        }
    }

    // send again OTP if the Email is Not Verified
    async resendOtp(reqBody: ResendOtpDto): Promise<any> {
        try {
            const { email } = reqBody;
            let user = await this.ISRAEL_User.findOne({ email: encrypt(email) });

            if (!user) user = await this.ITTIHAD_User.findOne({ email: encrypt(email) });

            if (!user) return returnMessage('userNotFound', reqBody.site == 'israel-today' ? 'he' : 'ar');

            user.otp = Math.floor(100000 + Math.random() * 900000);
            user.otpExpire = Date.now() + 900000;
            await user.save();
            const site = user.site === 'israel-today' ? 'israelBackOffice' : 'ittihadBackOffice';
            await this.sendMailToOtpVerification(site, user.otp, decrypt(user.name), decrypt(user.email));
            return true;
        } catch (error) {
            console.log('error in resend OTP', error);
            return error.message;
        }
    }

    jwtTokenGenerator(user: any): any {
        return jwt.sign(
            {
                id: user['_id'],
                site: user['site'],
            },
            process.env.JWT_SECRET,
            { expiresIn: process.env.JWT_EXPIRE },
        );
    }

    // this function is used for the login the article page users
    async articlePageUserRegister(reqBody: ArticlePageRegisterUserDto): Promise<any> {
        try {
            let { email, password, name, site } = reqBody;
            if (!email || !password || !name)
                return returnMessage('fieldsMissing', site === 'israel-today' ? 'he' : 'ar');
            const otp = Math.floor(100000 + Math.random() * 900000);
            const otpExpire = Date.now() + 900000;
            let newUser: any;

            if (reqBody.site === 'israel-today') {
                const [userExist, row_id] = await Promise.all([
                    this.ISRAEL_User.findOne({
                        email: encrypt(reqBody.email),
                        site,
                    }).select('email emailVerified ip name site status'),
                    this.ISRAEL_User.countDocuments().lean(),
                ]);

                if (userExist && !userExist.emailVerified) {
                    if (Date.now() > +userExist['otpExpire']) {
                        this.sendMailToOtpVerification(
                            'israelBackOffice',
                            otp,
                            decrypt(userExist.name),
                            decrypt(userExist.email),
                        );
                        userExist['otp'] = otp;
                        userExist['otpExpire'] = otpExpire;
                        await userExist.save();
                    }

                    return {
                        emailVerified: false,
                        email,
                        user: decryptObjectFields(userExist, ['email', 'name']),
                        token: this.jwtTokenGenerator(userExist),
                    };
                }

                if (userExist) return returnMessage('userExist', 'he');

                password = await bcrypt.hash(password, 12);
                // removed because of the otp verification
                // accountConfirmToken = crypto.randomBytes(32).toString('hex');

                newUser = await this.ISRAEL_User.create({
                    email: encrypt(email),
                    password,
                    site: reqBody.site,
                    ip: reqBody.ip,
                    device: reqBody.device,
                    name: encrypt(name.toLocaleLowerCase()),
                    row_id: row_id + 1,
                    otpExpire,
                    otp,
                });
            } else if (reqBody.site === 'ittihad-today') {
                const [userExist, row_id] = await Promise.all([
                    this.ITTIHAD_User.findOne({
                        email: encrypt(reqBody.email),
                        site,
                    }).select(' email emailVerified ip name site status'),
                    this.ITTIHAD_User.countDocuments().lean(),
                ]);

                if (userExist && !userExist.emailVerified) {
                    if (Date.now() > +userExist['otpExpire']) {
                        this.sendMailToOtpVerification(
                            'ittihadBackOffice',
                            otp,
                            decrypt(userExist.name),
                            decrypt(userExist.email),
                        );
                        userExist['otp'] = otp;
                        userExist['otpExpire'] = otpExpire;
                        await userExist.save();
                    }

                    return {
                        emailVerified: false,
                        email,
                        user: decryptObjectFields(userExist, ['email', 'name']),
                        token: this.jwtTokenGenerator(userExist),
                    };
                }

                if (userExist) return returnMessage('userExist', 'ar');

                password = await bcrypt.hash(password, 12);
                // removed because of the otp verification
                // accountConfirmToken = crypto.randomBytes(32).toString('hex');

                newUser = await this.ITTIHAD_User.create({
                    email: encrypt(email),
                    password,
                    site: reqBody.site,
                    ip: reqBody.ip,
                    device: reqBody.device,
                    name: encrypt(name.toLocaleLowerCase()),
                    row_id: row_id + 1,
                    otp,
                    otpExpire,
                });
            }

            if (!newUser) return returnMessage('default', reqBody.site == 'israel-today' ? 'he' : 'ar');

            // removed account verification
            // link = `${process.env.SERVER_HOST}/api/v1/user/verify-account?token=${accountConfirmToken}&email=${encrypt(
            //     email,
            // )}`;

            // const pageObj = {
            //     site: 'israelBackOffice',
            // };
            // if (newUser.site === 'ittihad-today') pageObj.site = 'ittihadBackOffice';

            this.sendMailToOtpVerification(
                newUser.site === 'israel-today' ? 'israelBackOffice' : 'ittihadBackOffice',
                otp,
                name,
                email,
            );

            return {
                user: decryptObjectFields(
                    {
                        email: newUser.email,
                        emailVerified: newUser.emailVerified,
                        ip: newUser.ip,
                        name: newUser.name,
                        site: newUser.site,
                        status: newUser.status,
                        _id: newUser?._id,
                    },
                    ['email', 'name'],
                ),
                token: this.jwtTokenGenerator(newUser),
                email,
            };
        } catch (error) {
            console.log('error while creating new article page user', error);
            return error.message;
        }
    }

    async sendMailToOtpVerification(site: string, otp: number, name: string, email: string): Promise<any> {
        const pageSetting = await this.PageSettingModel.findOne({ site }).lean();

        if (
            pageSetting.confirm_email_message === '' ||
            pageSetting.confirm_email_from === '' ||
            pageSetting.confirm_email_sub === '' ||
            pageSetting.confirm_email_reply === '' ||
            pageSetting.terms_privacy_policy_url === ''
        )
            return returnMessage('confirmEmailTemplateNotAvailable');

        pageSetting.confirm_email_message = pageSetting.confirm_email_message.replaceAll('{{first name}}', name);

        pageSetting.confirm_email_message = pageSetting.confirm_email_message.replaceAll('{{otp}}', otp.toString());

        pageSetting.confirm_email_message = pageSetting.confirm_email_message.replaceAll(
            '{{terms page URL}}',
            pageSetting.terms_privacy_policy_url,
        );

        this.sendMail(
            pageSetting.confirm_email_from,
            email,
            pageSetting.confirm_email_reply,
            pageSetting.confirm_email_sub,
            pageSetting.confirm_email_message,
        );
        return true;
    }

    async verifyUserAccount(reqBody: EmailVerifyUserDto): Promise<any> {
        try {
            const { email, token } = reqBody;
            const searchObj = {
                email,
                accountConfirmToken: crypto.createHash('sha256').update(token).digest('hex'),
            };
            let userExist = await this.ISRAEL_User.findOne(searchObj);

            if (!userExist) userExist = await this.ITTIHAD_User.findOne(searchObj);

            if (!userExist) return returnMessage('invalidLink');

            userExist.emailVerified = true;
            userExist.accountConfirmToken = undefined;
            await userExist.save();

            return true;
        } catch (error) {
            console.log('error in verify article page user account', error);
        }
    }

    async loginArticlePageUser(reqBody: LoginArticlePageDto): Promise<any> {
        try {
            const { email, password, ip, device } = reqBody;
            if (!email || !password)
                return returnMessage('emailPassNotFound', reqBody.site == 'israel-today' ? 'he' : 'ar');
            const searchObj = {
                email: encrypt(email),
                site: reqBody.site,
            };
            let user = await this.ISRAEL_User.findOne(searchObj);
            if (!user) user = await this.ITTIHAD_User.findOne(searchObj);
            if (user?.loggedInViaGoogle === true) {
                return returnMessage('userNotFound', reqBody.site == 'israel-today' ? 'he' : 'ar');
            }
            if (!user) return returnMessage('userNotFound', reqBody.site == 'israel-today' ? 'he' : 'ar');

            // if (!user.emailVerified) return returnMessage('notVerified');
            if (user.status === 'inActive')
                return returnMessage('userInActive', reqBody.site == 'israel-today' ? 'he' : 'ar');

            const passwordMatched = await bcrypt.compare(password, user.password);

            if (!passwordMatched)
                return returnMessage('incorrectEmailPassword', reqBody.site == 'israel-today' ? 'he' : 'ar');

            user.ip = ip;
            user.device = device;
            user.lastSeen = new Date();

            await user.save();
            const site = user.site === 'israel-today' ? 'israelBackOffice' : 'ittihadBackOffice';
            const pageSetting = await this.PageSettingModel.findOne({ site }).select('login_message').lean();
            return {
                message: pageSetting.login_message,
                token: this.jwtTokenGenerator(user),
                user: decryptObjectFields(
                    {
                        _id: user._id,
                        email: user.email,
                        emailVerified: user.emailVerified,
                        ip: user.ip,
                        name: user.name,
                        site: user.site,
                        status: user.status,
                    },
                    ['name', 'email'],
                ),
            };
        } catch (error) {
            console.log('error while user login in article page', error.message);
        }
    }

    async googleSignIn(reqBody: GoogleSignInDto): Promise<any> {
        try {
            const { googleAuthToken, site, ip, device } = reqBody;
            if (!googleAuthToken || !site)
                return returnMessage('googelAuthTokenNotFound', site == 'israel-today' ? 'he' : 'ar');

            const decode: any = jwt.decode(googleAuthToken);
            let userExist: ArticlePageUserDocument;
            if (site === 'israel-today') {
                userExist = await this.ISRAEL_User.findOne({ email: encrypt(decode.email) });
            } else if (site === 'ittihad-today') {
                userExist = await this.ITTIHAD_User.findOne({ email: encrypt(decode.email) });
            }

            if (!userExist) {
                const newUserObj = {
                    email: encrypt(decode.email),
                    name: encrypt(decode.name.toLocaleLowerCase()),
                    image: decode.picture,
                    googleData: JSON.stringify(decode),
                    emailVerified: true,
                    loggedInViaGoogle: true,
                    ip,
                    device,
                    lastSeen: new Date(),
                };
                if (reqBody.site === 'israel-today') {
                    const rowId = await this.ISRAEL_User.countDocuments();
                    newUserObj['site'] = 'israel-today';
                    newUserObj['row_id'] = rowId + 1;
                    userExist = await this.ISRAEL_User.create(newUserObj);
                } else if (reqBody.site === 'ittihad-today') {
                    const rowId = await this.ITTIHAD_User.countDocuments();
                    newUserObj['site'] = 'ittihad-today';
                    newUserObj['row_id'] = rowId + 1;
                    userExist = await this.ITTIHAD_User.create(newUserObj);
                }

                if (!userExist) return returnMessage('default', site == 'israel-today' ? 'he' : 'ar');
            }

            userExist.ip = ip;
            userExist.device = device;
            userExist.lastSeen = new Date();
            userExist.image = decode.picture;

            await userExist.save();
            const backOfficeSite = reqBody.site === 'israel-today' ? 'israelBackOffice' : 'ittihadBackOffice';
            const pageSetting = await this.PageSettingModel.findOne({ site: backOfficeSite }).lean();

            return {
                token: this.jwtTokenGenerator(userExist),
                user: decryptObjectFields(
                    {
                        email: userExist.email,
                        emailVerified: userExist.emailVerified,
                        ip: userExist.ip,
                        name: userExist.name,
                        site: userExist.site,
                        status: userExist.status,
                        _id: userExist?._id,
                        image: userExist?.image,
                    },
                    ['name', 'email'],
                ),
                message: pageSetting.login_message,
            };
        } catch (error) {
            console.log('error while user login in article page', error.message);
        }
    }

    async verifyOtpForArticlePage(verifyOtp: VerifyOtpDto): Promise<any> {
        try {
            let isOtpValid = await this.ISRAEL_User.findOne({
                email: encrypt(verifyOtp.email),
                otp: verifyOtp.otp,
                site: verifyOtp.site,
            }).select('-password');

            if (!isOtpValid) {
                isOtpValid = await this.ITTIHAD_User.findOne({
                    email: encrypt(verifyOtp.email),
                    otp: verifyOtp.otp,
                    site: verifyOtp.site,
                }).select('-password');
            }

            if (!isOtpValid) return returnMessage('otpNotValid', verifyOtp.site == 'israel-today' ? 'he' : 'ar');

            if (Date.now() > +isOtpValid['otpExpire'])
                return returnMessage('otpExpired', verifyOtp.site == 'israel-today' ? 'he' : 'ar');

            isOtpValid['otp'] = undefined;
            isOtpValid['otpExpire'] = undefined;
            isOtpValid['emailVerified'] = true;
            isOtpValid.lastSeen = new Date();
            await isOtpValid.save();

            const site = isOtpValid.site === 'israel-today' ? 'israelBackOffice' : 'ittihadBackOffice';
            const pageSetting = await this.PageSettingModel.findOne({ site }).select('registration_message').lean();

            return {
                data: decryptObjectFields(
                    {
                        email: isOtpValid.email,
                        emailVerified: isOtpValid.emailVerified,
                        ip: isOtpValid.ip,
                        name: isOtpValid.name,
                        site: isOtpValid.site,
                        status: isOtpValid.status,
                        _id: isOtpValid?._id,
                    },
                    ['name', 'email'],
                ),
                token: this.jwtTokenGenerator(isOtpValid),
                message: pageSetting.registration_message,
            };
        } catch (error) {
            console.log('error in verify the otp', error);
            return error.message;
        }
    }

    async verifyOtp(verifyOtp: VerifyOtpDto): Promise<any> {
        try {
            const isOtpValid = await this.Admin.findOne({
                email: encrypt(verifyOtp.email),
                otp: verifyOtp.otp,
                is_deleted: false,
            })
                .select('-password')
                .populate('user_type');

            if (!isOtpValid) return returnMessage('otpNotValid');

            if (Date.now() > +isOtpValid.otpExpire) return returnMessage('otpExpired');

            isOtpValid.otp = undefined;
            isOtpValid.otpExpire = undefined;
            isOtpValid.lastSeen = new Date();
            await isOtpValid.save();

            const token = this.jwtTokenGenerator(isOtpValid);

            let pendingPageSearchObj: object = {
                    israelStatus: 'pending',
                },
                pendingCommentSearchObj: object = {
                    israelStatus: 'pending',
                };

            if (isOtpValid.site === 'ittihadBackOffice') {
                pendingPageSearchObj = {
                    ittihadStatus: 'pending',
                };

                pendingCommentSearchObj = {
                    ittihadStatus: 'pending',
                };
            }

            const [pendingPageCount, pendingCommnetCount, pendingReplyComment] = await Promise.all([
                this.Page.countDocuments(pendingPageSearchObj),
                this.CommnetsModel.countDocuments(pendingCommentSearchObj),
                this.CommentReplyModel.countDocuments(pendingCommentSearchObj),
            ]);

            return {
                data: decryptObjectFields(isOtpValid, ['firstname', 'lastname', 'email', 'phone']),
                token,
                pendingPageCount,
                pendingCommnetCount: pendingCommnetCount + pendingReplyComment,
            };
        } catch (error) {
            console.log('error in verify the otp', error);
            return error.message;
        }
    }

    async forgotPassword(data: ForgotPasswordDto): Promise<any> {
        try {
            const user = await this.Admin.findOne({ email: encrypt(data.email) });

            if (!user) return returnMessage('userNotFound');
            const resetToken = crypto.randomBytes(32).toString('hex');

            user.passwordResetToken = crypto.createHash('sha256').update(resetToken).digest('hex');

            user.passwordResetExpires = Date.now() + 3600000;
            await user.save();

            const link = `${process.env.WEBHOST}/reset-password?token=${resetToken}`;

            const subject = 'Forgot Password Email';
            const message = `<tr>
      <td
        style="padding: 15px 15px 10px; color:#333333; font-family: Arial, Helvetica, sans-serif; font-size:15px; line-height:21px; text-align:left;">
        Click on the following link or copy and paste it into your web browser:<br><a
          href=${link} style="color: blue;">[Verification Link]</a></td>
    </tr>
    <tr>
      <td
        style="padding: 15px 15px 10px; color:#333333; font-family: Arial, Helvetica, sans-serif; font-size:15px; line-height:21px; text-align:left;">
        You'll be directed to a reset-password page.</td>
    </tr>`;
            const body = emailTemplate(message, link, decrypt(user.firstname));

            this.sendMail('', decrypt(user.email), '', subject, body);

            return true;
        } catch (error) {
            console.log('error in forgot password api', error.message);
            return error.message;
        }
    }

    async resetPassword(resetPasswordDto: ResetPasswordDto): Promise<any> {
        try {
            const { token, password } = resetPasswordDto;

            if (!token || !password) return returnMessage('tokenOrPasswordMissing');

            const hashedToken = crypto.createHash('sha256').update(token).digest('hex');

            let user = await this.Admin.findOne({
                passwordResetToken: hashedToken,
            }).select('email password');

            if (!user) return returnMessage('invalidToken');

            if (Date.now() > user.passwordResetExpires) return returnMessage('linkExpired');

            // const isSamePassword = await bcrypt.compare(password, user.password);
            // if (isSamePassword) return returnMessage('oldNewPasswordSame');

            const hash = await bcrypt.hash(password, 12);

            user.password = hash;
            user.passwordResetToken = undefined;
            user.passwordResetExpires = undefined;
            await user.save();

            return true;
        } catch (error) {
            console.log('error in reset password', error);
            return error.message;
        }
    }

    async forgotPasswordForArticleUser(data: ForgotPasswordDto): Promise<any> {
        try {
            let user = await this.ISRAEL_User.findOne({
                email: encrypt(data.email),
                site: data.site,
                loggedInViaGoogle: false,
            });

            if (!user)
                user = await this.ITTIHAD_User.findOne({
                    email: encrypt(data.email),
                    site: data.site,
                    loggedInViaGoogle: false,
                });

            if (!user) return returnMessage('userNotFound', data.site == 'israel-today' ? 'he' : 'ar');
            if (user.status === 'inActive')
                return returnMessage('userInActive', data.site == 'israel-today' ? 'he' : 'ar');
            if (!user.emailVerified) return returnMessage('notVerified', data.site == 'israel-today' ? 'he' : 'ar');
            // const resetToken = crypto.randomBytes(32).toString('hex');

            // user.passwordResetToken = crypto.createHash('sha256').update(resetToken).digest('hex');

            // user.passwordResetExpires = Date.now() + 3600000;

            user.passWordResetOtp = Math.floor(100000 + Math.random() * 900000);
            user.passWordResetOtpExpire = Date.now() + 900000;
            await user.save();

            this.sendResetPasswordEmail(user);

            return true;
        } catch (error) {
            console.log('error in forgot password api', error.message);
            return error.message;
        }
    }

    async sendResetPasswordEmail(user: any): Promise<any> {
        try {
            const searchObj = {
                site: 'israelBackOffice',
            };
            if (user.site === 'ittihad-today') searchObj.site = 'ittihadBackOffice';
            const pageSetting = await this.PageSettingModel.findOne(searchObj).lean();
            // const link = `${process.env.WEBHOST}/reset-password?token=${resetToken}`;
            if (
                !pageSetting ||
                pageSetting.reset_email_message === '' ||
                pageSetting.reset_email_from === '' ||
                pageSetting.reset_email_reply === '' ||
                pageSetting.reset_email_sub === ''
            )
                return returnMessage('resetEmailTemplateNotAvailable');

            pageSetting.reset_email_message = pageSetting.reset_email_message.replaceAll(
                '{{first name}}',
                decrypt(user.name),
            );

            pageSetting.reset_email_message = pageSetting.reset_email_message.replaceAll(
                '{{otp}}',
                user.passWordResetOtp,
            );

            this.sendMail(
                pageSetting.reset_email_from,
                decrypt(user.email),
                pageSetting.reset_email_reply,
                pageSetting.reset_email_sub,
                pageSetting.reset_email_message,
            );
            return;
        } catch (error) {
            console.log('error in the send reset password email ', error);
            return error.message;
        }
    }

    async resetPasswordForArticleUser(resetPasswordDto: ResetPasswordArticlePageDto): Promise<any> {
        try {
            const { otp, email, password } = resetPasswordDto;

            if (!otp || !password) return returnMessage('tokenOrPasswordMissing');

            let user = await this.ISRAEL_User.findOne({
                email: encrypt(email),
                passWordResetOtp: otp,
                loggedInViaGoogle: false,
                site: resetPasswordDto.site,
            }).select('email password site');

            if (!user)
                user = await this.ITTIHAD_User.findOne({
                    email: encrypt(email),
                    passWordResetOtp: otp,
                    loggedInViaGoogle: false,
                    site: resetPasswordDto.site,
                }).select('email password site');

            if (!user) return returnMessage('otpNotValid', resetPasswordDto.site == 'israel-today' ? 'he' : 'ar');

            if (Date.now() > user.passWordResetOtpExpire)
                return returnMessage('otpExpired', resetPasswordDto.site == 'israel-today' ? 'he' : 'ar');

            // const isSamePassword = await bcrypt.compare(password, user.password);
            // if (isSamePassword) return returnMessage('oldNewPasswordSame');

            const hash = await bcrypt.hash(password, 12);

            user.password = hash;
            user.passWordResetOtp = undefined;
            user.passWordResetOtpExpire = undefined;
            await user.save();
            const site = user.site === 'israel-today' ? 'israelBackOffice' : 'ittihadBackOffice';
            const pageSetting = await this.PageSettingModel.findOne({ site }).select('reset_message').lean();
            return { message: pageSetting.reset_message };
        } catch (error) {
            console.log('error in reset password', error);
            return error.message;
        }
    }

    sendMail(fromEmail: string, toMail: string, replyToEmail: string, subject: string, message: string): Promise<any> {
        try {
            console.log(769, 'inside the mail send');
            if (fromEmail === '') fromEmail = process.env.SUPPORT_EMAIL;
            if (replyToEmail === '') replyToEmail = process.env.SUPPORT_EMAIL;
            this.mailerService.sendMail({
                to: toMail,
                from: fromEmail,
                subject,
                html: message,
                replyTo: replyToEmail,
            });
            return;
        } catch (error) {
            console.log('error in email sent', error);
            return error.message;
        }
    }

    // this api is used for the listing of the article page of the users in the Back Office
    async usersList(request: any, searchObj: CreateSearchObjectDto): Promise<any> {
        try {
            const permissions = request.user.permissions;
            if (!permissions.users.read) return returnMessage('permissionDenied');

            // if (searchObj.sortField === 'totalComments') searchObj.sortField = 'updatedAt';
            const pagination = paginationObject(searchObj);

            let users: any[], totalUsers: number, totalComments: any[], totalReplyComments: any[];
            const queryObj = {};

            if (request.user.site === 'israelBackOffice') queryObj['site'] = 'israel-today';
            else if (request.user.site === 'ittihadBackOffice') queryObj['site'] = 'ittihad-today';

            if (
                searchObj.sortField === 'totalComments' ||
                searchObj.sortField === 'email' ||
                searchObj.sortField === 'name'
            )
                pagination.sort = {};
            if (searchObj.search && searchObj.search !== '') {
                queryObj['$or'] = [
                    {
                        status: {
                            $regex: searchObj.search.replace(/[^a-zA-Z0-9 ]/g, ''),
                            $options: 'i',
                        },
                    },
                    {
                        name: {
                            $regex: encrypt(searchObj.search.toLowerCase()),
                            $options: 'i',
                        },
                    },
                    {
                        site: {
                            $regex: searchObj.search.replace(/[^a-zA-Z0-9 ]/g, ''),
                            $options: 'i',
                        },
                    },
                    {
                        email: {
                            $regex: encrypt(searchObj.search.toLowerCase()),
                            $options: 'i',
                        },
                    },
                    {
                        device: {
                            $regex: searchObj.search.replace(/[^a-zA-Z0-9 ]/g, ''),
                            $options: 'i',
                        },
                    },
                ];

                const keywordType = getKeywordType(searchObj.search);
                if (keywordType === 'number') {
                    const numericKeyword = parseInt(searchObj.search);
                    queryObj['$or'].push({ row_id: numericKeyword });
                } else if (keywordType === 'date') {
                    const dateKeyword = new Date(searchObj.search);
                    queryObj['$or'].push({ updatedAt: dateKeyword });
                    queryObj['$or'].push({ lastSeen: dateKeyword });
                }
            }
            if (request.user.site === 'israelBackOffice') {
                [users, totalUsers, totalComments, totalReplyComments] = await Promise.all([
                    this.ISRAEL_User.find(queryObj).sort(pagination.sort).select('-password').lean(),
                    this.ISRAEL_User.countDocuments(queryObj),
                    this.CommnetsModel.find({ site: 'israel-today', userId: { $exists: true } }).lean(),
                    this.CommentReplyModel.find({ site: 'israel-today', userId: { $exists: true } }).lean(),
                ]);
            } else if (request.user.site === 'ittihadBackOffice') {
                [users, totalUsers, totalComments, totalReplyComments] = await Promise.all([
                    this.ITTIHAD_User.find(queryObj).sort(pagination.sort).select('-password').lean(),
                    this.ITTIHAD_User.countDocuments(queryObj),
                    this.CommnetsModel.find({ site: 'ittihad-today', userId: { $exists: true } }).lean(),
                    this.CommentReplyModel.find({ site: 'ittihad-today', userId: { $exists: true } }).lean(),
                ]);
            }

            totalComments = [...totalComments, ...totalReplyComments];

            users.forEach((user) => {
                user['totalComments'] = totalComments.filter(
                    (comment) => comment?.userId?.toString() === user?._id?.toString(),
                ).length;
            });

            users = decryptArrayOfObject(users, ['email', 'name', 'lastname']);

            if (searchObj.sortField === 'totalComments') {
                if (searchObj.sortOrder === 'asc') {
                    users = users.sort((a, b) => a.totalComments - b.totalComments);
                } else {
                    users = users.sort((a, b) => b.totalComments - a.totalComments);
                }
            }

            if (searchObj.sortField === 'email') {
                if (searchObj.sortOrder === 'asc') {
                    users = users.sort((a, b) => a.email.localeCompare(b.email));
                } else {
                    users = users.sort((a, b) => b.email.localeCompare(a.email));
                }
            }

            if (searchObj.sortField === 'name') {
                if (searchObj.sortOrder === 'asc') {
                    users = users.sort((a, b) => a.name.localeCompare(b.name));
                } else {
                    users = users.sort((a, b) => b.name.localeCompare(a.name));
                }
            }
            const page = pagination.page;
            const pageSize = pagination.resultPerPage;

            const startIndex = (page - 1) * pageSize;
            const endIndex = startIndex + pageSize;

            return {
                users: users.slice(startIndex, endIndex),
                totalPage: Math.ceil(totalUsers / pagination.resultPerPage) || 0,
            };
        } catch (error) {
            console.log('error in users list', error);
            return error.message;
        }
    }

    async getUser(request: any, userId: string): Promise<ArticlePageUserDocument> {
        try {
            const permissions = request.user.permissions;
            if (!permissions.users.read) return returnMessage('permissionDenied');
            let user: ArticlePageUserDocument, comments: any[];
            if (request.user.site === 'israelBackOffice') {
                [user, comments] = await Promise.all([
                    this.ISRAEL_User.findById(userId).lean(),
                    this.CommnetsModel.find({ userId, site: 'israel-today' }).lean(),
                ]);
            } else if (request.user.site === 'ittihadBackOffice') {
                [user, comments] = await Promise.all([
                    this.ITTIHAD_User.findById(userId).lean(),
                    this.CommnetsModel.find({ userId, site: 'ittihad-today' }).lean(),
                ]);
            }
            if (!user) return returnMessage('userNotFound');
            user.email = decrypt(user.email);
            user.name = decrypt(user.name);
            user['totalComments'] = comments.length;
            user['approvedComments'] = comments.filter((comment) => comment.status === 'approved').length;
            user['notApprovedComments'] = comments.filter((comment) => comment.status === 'notApproved').length;
            user['pendingComments'] = comments.filter((comment) => comment.status === 'pending').length;

            return user;
        } catch (error) {
            console.log('error in get user api', error);
            return error.message;
        }
    }

    async updateUser(request: any, userId: string, status: string): Promise<ArticlePageUserDocument> {
        try {
            const permissions = request.user.permissions;
            if (!permissions.users.write) return returnMessage('permissionDenied');
            let user: ArticlePageUserDocument;
            status === 'active' ? (status = 'active') : (status = 'inActive');
            if (request.user.site === 'israelBackOffice')
                user = await this.ISRAEL_User.findByIdAndUpdate(userId, { status }, { new: true }).lean();
            else if (request.user.site === 'ittihadBackOffice')
                user = await this.ITTIHAD_User.findByIdAndUpdate(userId, { status }, { new: true }).lean();

            if (!user) return returnMessage('userNotFound');
            user.email = decrypt(user.email);
            user.name = decrypt(user.name);

            return user;
        } catch (error) {
            console.log('error in update user api', error);
            return error.message;
        }
    }

    async pendingCounts(user: any): Promise<any> {
        let pendingPageSearchObj: object = {
                israelStatus: 'pending',
            },
            pendingCommentSearchObj: object = { israelStatus: 'pending' };

        if (user.site === 'ittihadBackOffice') {
            pendingPageSearchObj = {
                ittihadStatus: 'pending',
            };
            pendingCommentSearchObj = { ittihadStatus: 'pending' };
        }
        const [pendingPageCount, pendingCommnetCount, pendingReplyCommentCount] = await Promise.all([
            this.Page.countDocuments(pendingPageSearchObj),
            this.CommnetsModel.countDocuments(pendingCommentSearchObj),
            this.CommentReplyModel.countDocuments(pendingCommentSearchObj),
        ]);
        return {
            pendingPageCount,
            pendingCommnetCount: pendingCommnetCount + pendingReplyCommentCount,
        };
    }

    async downloadExcel(request: any): Promise<ArticlePageUserDocument> {
        try {
            let users: any;
            let totalComments: any[];
            let pipeline = {
                _id: 1,
                site: 1,
                name: 1,
                status: 1,
                row_id: 1,
                device: 1,
                email: 1,
                updatedAt: 1,
                lastSeen: 1,
                createdAt: 1,
                // totalComments: 1,
            };
            if (request.user.site === 'israelBackOffice') {
                [users, totalComments] = await Promise.all([
                    this.ISRAEL_User.find().select(pipeline).lean(),
                    this.CommnetsModel.find({ site: 'israel-today', userId: { $exists: true } })
                        .select('userId')
                        .lean(),
                ]);
            } else if (request.user.site === 'ittihadBackOffice') {
                [users, totalComments] = await Promise.all([
                    this.ITTIHAD_User.find().select(pipeline).lean(),
                    this.CommnetsModel.find({ site: 'ittihad-today', userId: { $exists: true } })
                        .select('userId')
                        .lean(),
                ]);
            }
            users.forEach((user) => {
                user['totalComments'] = totalComments.filter(
                    (comment) => comment.userId.toString() === user._id.toString(),
                ).length;
            });
            users = decryptArrayOfObject(users, ['email', 'name']);
            return this.optimiseExcel(users);
        } catch (error) {
            console.log('error in update user api', error);
            return error.message;
        }
    }

    optimiseExcel(data: any[]): any {
        try {
            const batchSize = Math.ceil(Math.sqrt(data.length));
            let chunkArray: any[] = [];
            for (let index = 0; index < batchSize; index++) {
                chunkArray.push(data.splice(0, batchSize));
            }

            let updatedData = chunkArray.map((element: any) => {
                return this.optimisedUserXls(element);
            });
            return [].concat.apply([], updatedData);
        } catch (error) {
            console.log('error while exporting users data', error);
            return error.message;
        }
    }

    optimisedUserXls(data: any[]): any {
        try {
            const newArrayData: any[] = [];
            data.forEach((element: any) => {
                let obj = {};
                obj['Id'] = element.row_id || '-';
                obj['Status'] = element.status === 'active' ? 'Active' : 'In-Active';
                obj['Name'] = capitalizeFirstLetter(element.name) || '-';
                obj['Site'] = element.site === 'israel-today' ? 'Israel-Today' : 'Ittihad-Today';
                obj['Comment'] = element.totalComments || '-';
                obj['Lastseen'] = element.lastSeen
                    ? moment(element.lastSeen).tz('Israel').format('DD.MM.YYYY HH:mm')
                    : '-';
                obj['Registration'] = element.createdAt
                    ? moment(element.createdAt).tz('Israel').format('DD.MM.YYYY HH:mm')
                    : '-';
                obj['Email'] = element.email || '-';
                obj['Device'] = element.device || '-';

                newArrayData.push(obj);
            });

            return newArrayData;
        } catch (error) {
            console.log('error while exporting users data', error);
            return error.message;
        }
    }

    async verifyArticlePageAuthToken(token: any): Promise<any> {
        try {
            let currentUser: any;

            const decoded: any = jwt.verify(token, process.env.JWT_SECRET);
            if (decoded['site'] === 'israel-today') {
                currentUser = await this.ISRAEL_User.findById(decoded.id);
            } else if (decoded['site'] === 'ittihad-today') {
                currentUser = await this.ITTIHAD_User.findById(decoded.id);
            }
            if (!currentUser) return returnMessage('userNotFound');

            currentUser.lastSeen = new Date();
            await currentUser.save();

            return currentUser;
        } catch (error) {
            console.log('error in verify article page user token', error);
            return returnMessage('tokenNotExistOnArticle');
        }
    }

    async cron(time: any): Promise<any> {
        try {
            if (!time || isNaN(time)) return 'time should be in minutes';
            if (time) {
                console.log('Cron Job Started');
                const currentTime = moment().utc().tz('Asia/Kolkata');
                const pastTime = moment().utc().tz('Asia/Kolkata').subtract(parseInt(time), 'minutes');
                const pastCronTime = moment().utc().tz('Asia/Kolkata').subtract(parseInt(time), 'minutes');
                console.log(currentTime, pastTime, pastCronTime);
                const [comments, replyComments, israelPageSetting, ittihadPageSetting, pages] = await Promise.all([
                    this.CommnetsModel.find().lean(),
                    this.CommentReplyModel.find().lean(),
                    this.PageSettingModel.findOne({ site: 'israelBackOffice' }).lean(),
                    this.PageSettingModel.findOne({ site: 'ittihadBackOffice' }).lean(),
                    this.Page.find({ $or: [{ israelStatus: 'active' }, { ittihadStatus: 'active' }] }).lean(),
                ]);
                console.log(pages.length, 46);
                const totalComments = [...comments, ...replyComments];
                let israelTableRow = `
            <thead>
		        <tr>
                    <td style="font-weight: bolder; padding: 15px; border: 1px solid black;">#</td>
                    <td style="font-weight: bolder; padding: 15px; border: 1px solid black;">Page name</td>
                    <td style="font-weight: bolder; padding: 15px; border: 1px solid black;">Page URL</td>
                    <td style="font-weight: bolder; padding: 15px; border: 1px solid black;">No of new comment</td>
		        </tr>
	        </thead>`;

                let ittihadTableRow = `
            <thead>
		        <tr>
                    <td style="font-weight: bolder; padding: 15px; border: 1px solid black;">#</td>
                    <td style="font-weight: bolder; padding: 15px; border: 1px solid black;">Page name</td>
                    <td style="font-weight: bolder; padding: 15px; border: 1px solid black;">Page URL</td>
                    <td style="font-weight: bolder; padding: 15px; border: 1px solid black;">No of new comment</td>
		        </tr>
	        </thead>`;

                let israelCounter = 0,
                    ittihadCounter = 0;
                pages.forEach((page: any) => {
                    console.log('inside the israel comments loop');
                    if (page.israelStatus === 'active' && page.isrealUrl && page.israelPage) {
                        const comments = totalComments.filter(
                            (comment: any) =>
                                moment(comment.createdAt).tz('Asia/Kolkata').isSameOrAfter(pastTime) &&
                                moment(comment.createdAt).tz('Asia/Kolkata').isBefore(currentTime) &&
                                comment['site'] === 'israel-today' &&
                                comment['pageId'].toString() == page?._id?.toString(),
                        );
                        console.log(comments.length, 75);
                        if (comments.length !== 0) {
                            israelTableRow += `
                        <tr>
                        <td style="padding: 15px; border: 1px solid black;">${++israelCounter}</td>
                        <td style="padding: 15px; border: 1px solid black;">
                        <a style="color: blue; text-decoration: underline;" href="${process.env.WEBHOST}/pages/edit/${
                            page._id
                        }" target="_blank">${page['israelPage']}</a></td>
                        <td style="padding: 15px; border: 1px solid black;">
                        <a style="color: blue; text-decoration: underline;" href="${
                            page['isrealUrl']
                        }" target="_blank">${page['isrealUrl']}</a></td>
                        <td style="padding: 15px; border: 1px solid black;">${comments.length}</td>
                       </tr>`;
                        }
                    }
                    if (page.ittihadStatus === 'active' && page.ittihadUrl && page.ittihadPage) {
                        const comments = totalComments.filter(
                            (comment: any) =>
                                moment(comment.createdAt).tz('Asia/Kolkata').isSameOrAfter(pastCronTime) &&
                                comment['site'] === 'ittihad-today' &&
                                comment['pageId'].toString() == page._id.toString() &&
                                comment['ittihadStatus'] == 'approved',
                        );
                        if (comments.length !== 0) {
                            israelTableRow += `<tr>
                        <td style="padding: 15px; border: 1px solid black;">${++israelCounter}</td>
                        <td style="padding: 15px; border: 1px solid black;">
                        <a style="color: blue; text-decoration: underline;" href="${process.env.WEBHOST}/pages/edit/${
                            page._id
                        }" target="_blank">${page['ittihadPage']}</a></td>
                        <td style="padding: 15px; border: 1px solid black;">
                        <a style="color: blue; text-decoration: underline;" href="${
                            page['ittihadUrl']
                        }" target="_blank">${page['ittihadUrl']}</a></td>
                        <td style="padding: 15px; border: 1px solid black;">${comments.length}</td>
                        </tr>`;
                        }
                    }
                });

                pages.forEach((page: any) => {
                    if (page.ittihadStatus === 'active' && page.ittihadUrl && page.ittihadPage) {
                        const comments = totalComments.filter(
                            (comment: any) =>
                                moment(comment.createdAt).tz('Asia/Kolkata').isSameOrAfter(pastTime) &&
                                moment(comment.createdAt).tz('Asia/Kolkata').isBefore(currentTime) &&
                                comment['site'] === 'ittihad-today' &&
                                comment['pageId'].toString() == page._id.toString(),
                        );
                        if (comments.length !== 0) {
                            ittihadTableRow += `<tr>
                        <td style="padding: 15px; border: 1px solid black;">${++ittihadCounter}</td>
                        <td style="padding: 15px; border: 1px solid black;">
                        <a style="color: blue; text-decoration: underline;" href="${process.env.WEBHOST}/pages/edit/${
                            page._id
                        }" target="_blank">${page['ittihadPage']}</a></td>
                        <td style="padding: 15px; border: 1px solid black;">
                        <a style="color: blue; text-decoration: underline;" href="${
                            page['ittihadUrl']
                        }" target="_blank">${page['ittihadUrl']}</a></td>
                        <td style="padding: 15px; border: 1px solid black;">${comments.length}</td>
                        </tr>`;
                        }
                    }
                    if (page.israelStatus === 'active' && page.isrealUrl && page.israelPage) {
                        const comments = totalComments.filter(
                            (comment: any) =>
                                moment(comment.createdAt).tz('Asia/Kolkata').isSameOrAfter(pastCronTime) &&
                                comment['site'] === 'israel-today' &&
                                comment['pageId'].toString() == page._id.toString() &&
                                comment['israelStatus'] === 'approved',
                        );
                        console.log(comments.length, 140);
                        if (comments.length !== 0) {
                            ittihadTableRow += `<tr>
                        <td style="padding: 15px; border: 1px solid black;">${++ittihadCounter}</td>
                        <td style="padding: 15px; border: 1px solid black;">
                        <a style="color: blue; text-decoration: underline;" href="${process.env.WEBHOST}/pages/edit/${
                            page._id
                        }" target="_blank">${page['israelPage']}</a></td>
                        <td style="padding: 15px; border: 1px solid black;">
                        <a style="color: blue; text-decoration: underline;" href="${
                            page['isrealUrl']
                        }" target="_blank">${page['isrealUrl']}</a></td>
                        <td style="padding: 15px; border: 1px solid black;">${comments.length}</td>
                       </tr>`;
                        }
                    }
                });
                if (israelCounter > 0) {
                    let israelEmails = israelPageSetting.comment_notifications[0];
                    israelTableRow = `<table>${israelTableRow}</table>`;
                    israelPageSetting.newcomment_email_message = israelPageSetting.newcomment_email_message.replaceAll(
                        '{{comment_text}}',
                        israelTableRow,
                    );
                    israelPageSetting.newcomment_email_message = israelPageSetting.newcomment_email_message.replaceAll(
                        '{{site_name}}',
                        'Israel-Today',
                    );

                    JSON.parse(israelEmails).map((email: string) => {
                        this.sendMail(
                            israelPageSetting.newcomment_email_from,
                            email,
                            israelPageSetting.newcomment_email_reply,
                            israelPageSetting.newcomment_email_sub,
                            israelPageSetting.newcomment_email_message,
                        );
                    });
                }
                if (ittihadCounter > 0) {
                    let ittihadEmails = ittihadPageSetting.comment_notifications[0];
                    ittihadTableRow = `<table>${ittihadTableRow}</table>`;
                    ittihadPageSetting.newcomment_email_message =
                        ittihadPageSetting.newcomment_email_message.replaceAll('{{comment_text}}', ittihadTableRow);
                    ittihadPageSetting.newcomment_email_message =
                        ittihadPageSetting.newcomment_email_message.replaceAll('{{site_name}}', 'Ittihad-Today');
                    JSON.parse(ittihadEmails).map((email: string) => {
                        this.sendMail(
                            ittihadPageSetting.newcomment_email_from,
                            email,
                            ittihadPageSetting.newcomment_email_reply,
                            ittihadPageSetting.newcomment_email_sub,
                            ittihadPageSetting.newcomment_email_message,
                        );
                    });
                }
            }
        } catch (error) {
            console.log('error in the cron api call', error);
            return error.message;
        }
    }

    async captchaVerification(captchaBody: CaptchaVerificationDto): Promise<any> {
        try {
            const response = await axios.post(
                `https://www.google.com/recaptcha/api/siteverify?secret=${process.env.CAPTCHA_SECRET_KEY}&response=${captchaBody.token}`,
            );
            return response.data;
        } catch (error) {
            console.log('error in the caprcha verification', error);
            return error.message;
        }
    }
}
