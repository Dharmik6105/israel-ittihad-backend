import { Body, Controller, HttpStatus, Param, Post, Req, Res } from '@nestjs/common';
import { HistoryLogsService } from './history-logs.service';
import { Response } from 'express';
import { CustomError } from 'src/helpers/customError';
import { CreateSearchObjectDto } from 'src/common/search_object.dto';
import { ApiTags } from '@nestjs/swagger';

@Controller('api/v1/historyLogs')
@ApiTags('History Logs')
export class HistoryLogsController {
    constructor(private historyLogsService: HistoryLogsService) {}

    @Post('/getHistoryLogs/:id')
    async getHistoryLogs(
        @Body() searchObject: CreateSearchObjectDto,
        @Res() response: Response,
        @Param('id') logId: string,
        @Req() request,
    ) {
        try {
            let getHistoryLogs = await this.historyLogsService.getHistoryLogs(searchObject, logId, request);

            if (typeof getHistoryLogs === 'string')
                throw new CustomError({ success: false, message: getHistoryLogs }, 400);

            return response.status(HttpStatus.OK).json({
                success: true,
                data: getHistoryLogs,
            });
        } catch (err) {
            return response.status(err.status).json({
                success: false,
                message: err.response.message,
                error: err.response.message,
            });
        }
    }
}
